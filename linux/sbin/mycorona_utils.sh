#!/usr/bin/env bash 

# JH CSSE data directory hierarchy
WORKING_DIR="/var/lib/mycorona/data"
RAW_DATA_DIR="$WORKING_DIR/raw"
FINISHED_DIR="$WORKING_DIR/transform"
FRESH_STAMP="$WORKING_DIR/.fresh"

# datasets & utilites
CURRENT="global.current"
CURRENT_PATH="repo/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"

DEATH="global.deaths"
DEATH_PATH="repo/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"

CONFIRMED_US="us.confirmed"
CONFIRMED_US_PATH="repo/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv"

DEATH_US="us.deaths"
DEATH_US_PATH="repo/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv"


ALL_DATASETS="CURRENT DEATH CONFIRMED_US DEATH_US"


function dsetname(){
    # get dataset stub, e.g. CURRENT, DEATH
    dset=$1
    # use shell indirection to print the underlying variable
    echo "${!dset}"
}

function linkname(){
    # get dataset stub, e.g. CURRENT, DEATH
    dset=$1
    # use shell indirection to get the underlying variable
    dset_STUB=${!dset}
    # print value of variable with suffix
    echo "${dset_STUB}.csv"
}

function hashname(){
    # get dataset stub, e.g. CURRENT, DEATH
    dset=$1
    # use shell indirection to get the underlying variable, if it exists
    dset_STUB=${!dset}
    test -n "$dset_STUB" || { echo "ERROR: datasaet hash not detected"; return 1; }
    # print value of variable with suffix
    echo "${dset_STUB}.sha256"
}

function pathname(){
    # get dataset stub, e.g. CURRENT, DEATH
    dset=$1
    # resolve to a path variable, e.g CURRENT_PATH, DEATH_PATH
    _path=$dset"_PATH";dset_PATH=${!_path}
    # print value of variable with suffix
    echo "$dset_PATH"
}


# data warehouse directory hierarchy
WH_ROOT="/home/mycorona"
WAREHOUSE_INPUT="$WH_ROOT/input"
WAREHOUSE_OUTPUT="$WH_ROOT/output"

# hadoop environment 
INSTALL_ROOT="/home/atomsk/.local"
HADOOP="$INSTALL_ROOT/bin/hadoop"
HDFS="$INSTALL_ROOT/bin/hdfs"
JOB_CONFIG_DIR="/etc/mycorona/conf.d"
# function for updating the fresh stamp
function notify_fresh(){
    reducer_id_=$1
    outdir_=$2 
    echo "$reducer_id_,$outdir_" >> $FRESH_STAMP
}