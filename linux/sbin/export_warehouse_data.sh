#!/usr/bin/env bash 

# get utilities
. /usr/local/sbin/mycorona_utils.sh 

# final destination for transformed datasets
DESTINATION="$FINISHED_DIR/datasets"

# check for warehouse output dir
$HDFS dfs -ls $WAREHOUSE_OUTPUT 2> /dev/null || {
    echo "ERROR: Warehouse data output directory not found."
    echo "Skipping warehouse data export."
    exit 1
}

# check for fresh data indication
test -f $FRESH_STAMP || { 
    echo "No fresh data discovered in warehouse."
    echo "Skipping warehouse data export"
    exit 0;
}

# error output function
function err(){
    operation=$1
    echo "ERROR: Error occured while $operation."
    echo "ERROR: Skipping warehouse data export."
}

# proceed with data export 
echo "Exporting transformed data from warehouse."

# create destination directory if it doesn't exist
test -d $DESTINATION || { 
    echo "WARNING: Destination directory $DESTINATION doesn't exist! "
    echo -n "Creating destination directory $DESTINATION..."
    mkdir -p $DESTINATION &> /dev/null || {
        echo "failed."
        err "creating transformed data directory"
        exit 1
    }
    echo "success."
}

# use some python to read the .fresh file, which should provide some indication
# as to which output directories need to be exported.
# The .fresh file should contain lines of the form:
#       idx,outdir
#       idx,outdir
#       ... 
# where idx is a reducer index, and outdir is the warehouse directory containing 
# the output from that reducer. If multiple lines specify same reducer, only the 
# latest line will be used. Output directories that contain data from multiple 
# reducers are handles as well
get_fresh_dirs="
# open .fresh and extract raw outdir info, which 
# note that later entries with the same index overwrite earlier entries
dir_map={}
with open('$FRESH_STAMP','r') as outdirs:
    for line in outdirs:
        job,outdir=line.strip().split(',')
        dir_map[job]=outdir 
if not dir_map: raise ValueError('$FRESH_STAMP file contained no data')
# consolidate directories that contain output from multiple jobs 
reverse_map={}
for job, outdir in dir_map.items():
    if outdir not in reverse_map:
        reverse_map[outdir]=set()
    reverse_map[outdir].add(job)
# convert to records and print
records=[]
for outdir,jobs in reverse_map.items():
    all_red=','.join(sorted(jobs))
    record=';'.join([all_red,outdir])
    records.append(record)
if records: print(' '.join(records))
else: raise ValueError('$FRESH_STAMP file contents unrecognized')
"

# use some python to setup the shell environment from records that were produced 
# by the get_fresh_dirs script 
setup_export="
from argparse import ArgumentParser 
# get record from input
parser=ArgumentParser()
parser.add_argument('record'); args=parser.parse_args()
# extract constituent parts 
jobs,outdir=args.record.split(';')
jobs=' '.join(jobs.split(','))
# validate
if not (jobs and outdir): raise ValueError('Invalid record %s'%args.record)
print('export jobs=\"%s\"'%jobs)
print('export outdir=%s'%outdir)
"

# wrap the python script in a shell function to ensure it's called properly 
function setup_export_from_record() {
    record=$1 
    # exected output variables 
    jobs=
    outdir=
    echo "Setting up export environment from record: $record"
    # run some python, capture output and check for errors
    new_env=$(python3 -c "$setup_export" $record) || { 
        echo "Error parsing record $record: "
        echo "$new_env"
        return 1
    }
    # update environment from output and validate 
    eval "$new_env" || { 
        echo "Error evaluating environment from output: "
        echo "$new_env"
        return 1
    }
    test -n "$jobs" || {
        echo "ERROR: Record contained no reducer data"
        return 1 
    }
    test -n "$outdir" || {
        echo "ERROR: Record contained no output directory"
        return 1 
    }
}

FRESH_DIRS=$(python3 -c "$get_fresh_dirs" 2>&1) || { echo "ERROR:"; echo "$FRESH_DIRS"; err "reading $FRESH_STAMP"; exit 1; }
echo "Found fresh warehouse data ready for export: $FRESH_DIRS"

# get all directories in warehous output dir
echo "Getting warehouse export inventory..."
all_dirs=$($HDFS dfs -ls -C $WAREHOUSE_OUTPUT 2> /dev/null) || {
    err "locating data output directory"
    exit 1
}
all_dirs=$(echo $all_dirs)
all_dirs=$(python3 -c "from pathlib import Path; print(' '.join([Path(i).name for i in '$all_dirs'.split()]))")
echo "Warehouse directories ready for export: $all_dirs"
export_dirs=;
tmpdirs=;
for freshdir in $FRESH_DIRS
do 
    jobs=;outdir=;
    setup_export_from_record $freshdir || { err "setting up export environment"; exit 1; }
    freshdir_in_alldirs="e=0 if '$outdir' in '$all_dirs'.split() else 1; exit(e)"
    if python3 -c "$freshdir_in_alldirs"
    then 
        :
    else
        echo "$freshdir not eligible for warehouse export. Skipping."; continue;
    fi 
    wh_dir=$WAREHOUSE_OUTPUT/$outdir
    tmpdir="/tmp/$outdir"
    test -e "$tmpdir" && rm -rf "$tmpdir"
    echo "Scheduling $wh_dir export to $tmpdir"
    export_dirs="$wh_dir $export_dirs"
    tmpdirs="$tmpdir $tmpdirs"
done 

# export to temp dir
tmpdir="/tmp/"
# mkdir $tmpdir || { err "creating tmpdir $tmpdir"; exit 1; }
# tmpdirs="$outdir $tmpdirs"
pushd $tmpdir &> /dev/null
test -n "$export_dirs" || { echo "No data is scheduled for export. Exiting."; exit 0; }
echo "CHECK: Wanted warehouse data: $export_dirs"
echo "Retrieving data..."
$HDFS dfs -get $export_dirs $tmpdir || {
    echo "ERROR: Error occurred during data export."
    exit 1;
}
echo "Data retrieval completed."

ERROR=
tmpdirs=
wh_cleanup_dirs=
for freshdir in $FRESH_DIRS
do 
    jobs=;outdir=;
    setup_export_from_record $freshdir || { err "setting up export environment"; exit 1; }
    # consolidate pieces
    tmpdir="/tmp/$outdir"
    pushd $tmpdir &> /dev/null
    echo "Consolidating data in directory: $(pwd)"
    outfile="$outdir.dat"
    parts=$(ls part* 2> /dev/null)
    test -n "$parts" || {
        err "locating output data blocks"
        ERROR=true; break
    }
    echo "Found pieces: $parts"
    cat $parts > $outfile || {
        err "consolidating part files"
        ERROR=true; break
    }
    echo "Wrote dataset: $outfile"
    echo "Data consolidation completed."
    # move the consolidated dataset to the mycorona data directory
    echo -n "Relocating dataset to destination directory $DESTINATION ..."
    mv $outfile $DESTINATION &> /dev/null
    popd &> /dev/null
    pushd $FINISHED_DIR &> /dev/null
    dest_dataset="datasets/$outfile"
    test -f $dest_dataset || {
        echo "failed."
        echo "ERROR: Could not find dataset in destination directory."
        echo "ERROR: Export aborted."
        ERROR=true; break
    }
    echo "success."
    # create symlinks for quick access
    for id in $jobs
    do 
        data_link="dataset.$id.dat"
        echo "Linking data to set: $data_link..."
        old_data=;
        test -h "$data_link" && {
            echo "Found existing link. Removing."
            old_data=$(readlink $data_link)
            rm $data_link || { 
                err "removing data link $data_link"; 
                ERROR=true; break
            }
        }
        test -f "$old_data" && {
            echo "Found existing data. Removing."
            rm $old_data || {
                err "removing old data $old_data"; 
                ERROR=true; break
            }
        } 
        ln -s $dest_dataset $data_link || {
            echo "ERROR: Could not link data to set: $data_link"
            echo "ERROR: Export aborted."
            ERROR=true; break
        }
        # sha256 the data 
        data_hash="$data_link.sha256"
        sha256sum "$data_link" > "$data_hash" || {
            echo "ERROR: Could not create hash for dataset: $data_link"
            echo "ERROR: Export aborted."
            ERROR=true; break
        }
    done 
    popd &> /dev/null
    test -n "$ERROR" && break
    echo "Exported warehouse output: $outdir"
    test -n "$wh_cleanup_dirs" && wh_cleanup_dirs=$wh_cleanup_dirs","
    wh_cleanup_dirs="$wh_cleanup_dirs$outdir" 
    echo "Schedulied warehouse directory '$outdir' for cleanup."

done
echo "Cleaning warehouse directories: $wh_cleanup_dirs"
# expand the list of warehouse dirs to a list of cleanup services 
clean_jobs=$(eval "echo mycorona-warehouse-cleanup@{$wh_cleanup_dirs}")
echo "Launching cleanup jobs: $clean_jobs"
sleep 3
systemctl start $clean_jobs || {
    echo "WARNING: Error occurred during warehouse cleanup."
    echo "WARNING: Warehouse cleanup interrupted."
}

# cleanup temp dir and exit
if [ -n "$tmpdirs" ]; then 
    echo "Cleaning up temporary directories: $tmpdirs"
    pushd /tmp &> /dev/null || exit 1 
    rm -rf $tmpdirs || {
        echo "WARNING: Error occurred during temp dir cleanup."
        echo "WARNING: Temp dir cleanup aborted."
    }
    popd &> /dev/null
fi
test -n "$ERROR" && exit 1
rm -f $FRESH_STAMP || { 
    echo "ERROR: Error occured while removing freshstamp"
    exit 1
}
echo "Warehouse data export completed."
