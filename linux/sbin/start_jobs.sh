#!/bin/bash 

# start compile jobs 0 through N, where N is input on the command line 
last_job=$1 

wants_jobs="x-data-compile@{0..$last_job}"
wants_jobs=$(eval "echo $wants_jobs")

/bin/systemctl start $wants_jobs