#!/usr/bin/env bash 

# get shared utilities
. /usr/local/sbin/mycorona_utils.sh 

REPO_DIR="$RAW_DATA_DIR/repo"
DATA_REPO="https://github.com/CSSEGISandData/COVID-19.git"

# ARCHIVE_PATH=
# ARCHIVE_LINK=

# CURRENT_PATH="repo/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv"
# CURRENT_HASH="global.current.sha256"
# DEATH_PATH="repo/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv"
# DEATH_HASH="global.deaths.sha256"
# check for privileges 
touch /var/lib &> /dev/null || { echo "Must run as administrator"; exit 1; }

# check out the working dir if it doesnt exist
not_found="Data repository not found!"
clone="Cloning repository: $DATA_REPO"
dest="As data directory: $REPO_DIR"
test -d $REPO_DIR || { 
    echo "$not_found"; echo "$clone"; echo "$dest"
    git clone $DATA_REPO $REPO_DIR;
    echo "Repository fetch complete. Warehouse manager will be notified of fresh data retrieval."
    touch $FRESH_STAMP
}

pushd $RAW_DATA_DIR &> /dev/null || { echo "Error navigating working directory"; exit 1; }

# make sure dataset symlinks exist 
# check for new data
for dset in $ALL_DATASETS; 
do 
    # _link=$dset"_LINK"; _hash=$dset"_HASH"
    dset_LINK=$(linkname $dset)
    dset_PATH=$(pathname $dset)
    dset_NAME=$(dsetname $dset)
    dset_HASH=$(hashname $dset)
    # create symlinks to archive datasets if they don't exist
    test -h $dset_LINK || {
        echo -n "Symlinking $dset_NAME data..."
        test -f $dset_PATH || { echo "failed."; echo "Couldn't locate dataset $dset_PATH."; exit 1; }
        ln -s $dset_PATH $dset_LINK
        echo "success."
        # also generate sha256sum data 
        test -f $dset_HASH || { sha256sum $dset_LINK > $dset_HASH; }
    }
done 

# pull any new updates from upstream
pushd $REPO_DIR &> /dev/null || { echo "Error navigating data repository."; exit 1; }
echo "Fetching updates from CSSE repository"
git pull || { echo "Error updating data repository."; exit 1; }
popd &> /dev/null

# check for new data
for dset in $ALL_DATASETS; 
do 
    # _link=$dset"_LINK"; _hash=$dset"_HASH"
    dset_LINK=$(linkname $dset)
    dset_HASH=$(hashname $dset)
    # test if there were new updates, and notify the data warehouse
    echo "Checking $dset_LINK for fresh data"
    old_hash=$(cat $dset_HASH)
    sha256sum $dset_LINK > $dset_HASH;
    new_hash=$(cat $dset_HASH)
    test "$old_hash" != "$new_hash" && {
        echo "Fresh data detected during update. Notifying warehouse manager."
        touch $FRESH_STAMP
    }
done 

# update freshstamp permissions, just in case it was forced by the user
{ test -f $FRESH_STAMP && chmod o+w $FRESH_STAMP && echo "Fresh stamp is fresh!"; } || echo "No new data discovered."

popd &> /dev/null
