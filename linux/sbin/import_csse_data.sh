#!/usr/bin/env bash 

# get utilities
. /usr/local/sbin/mycorona_utils.sh 

# check for warehouse root dir
$HDFS dfs -ls $WH_ROOT || {
    echo "ERROR: Warehouse data root not found."
    echo "Try 'hdfs dfs -mkdir $WH_ROOT' to create it"
    exit 1
}

# clear data warehouse input 
$HDFS dfs -ls $WAREHOUSE_INPUT && {
    echo "Cleaning out stale warehouse data"
    $HDFS dfs -rm $WAREHOUSE_INPUT/*
    $HDFS dfs -rmdir $WAREHOUSE_INPUT
}

# push data to warehouse 
echo "Pushing fresh data to warehouse"
pushd $RAW_DATA_DIR &> /dev/null
$HDFS dfs -mkdir $WAREHOUSE_INPUT
data_links=;
for dset in $ALL_DATASETS
do 
    data_links="$(linkname $dset) $data_links"
done
test -n "$data_links" || { echo "ERROR: Error occurred while getting dataset links."; exit 1; }
echo "Pushing datasets: $data_links"
$HDFS dfs -put $data_links $WAREHOUSE_INPUT || {
    echo "Error moving data to warehouse"
    exit 1
}
# $HDFS dfs -put $DEATH_LINK $WAREHOUSE_INPUT || {
#     echo "Error moving death data to warehouse"
#     exit 1
# }
