#!/usr/bin/env bash 

# get utilities
. /usr/local/sbin/mycorona_utils.sh 

reducer=$1 
jobid=$1 
test -n $jobid || jobid=0;

# create the output directory if it doesn't exist
echo "Checking for warehouse data output directory..."
$HDFS dfs -ls $WAREHOUSE_OUTPUT || {
    echo "WARNING: Warehouse data output directory not found!"
    echo "WARNING: Creating warehouse data output directory"
    $HDFS dfs -mkdir $WAREHOUSE_OUTPUT
}

# create outdir name from timestamp
outdir=$(date -u +%Y%m%d.%H%M%S.%N)

# get job parameters from config using python
job_configs=$(ls $JOB_CONFIG_DIR)
setup_job="
from configparser import ConfigParser 
from os.path import join
# get config files from shell vars
configs='$job_configs'.split()
cfgdir='$JOB_CONFIG_DIR'
jobid='$jobid'
configs=[join(cfgdir,i) for i in configs]
# parse config files
cfg=ConfigParser()
cfg.read(configs)
# print variables 
msg='export %s=\"%s\"'
print(msg%('inputs',' '.join(cfg[jobid]['inputs'].split(','))))
print(msg%('outputdir',cfg[jobid]['outputdir']))
print(msg%('mappers',' '.join(cfg[jobid]['mappers'].split(','))))
print(msg%('reducers',' '.join(cfg[jobid]['reducers'].split(','))))
"
ENV=$(python -c "$setup_job" 2>&1) || { echo "$ENV"; exit 1; }
eval "$ENV" || { echo "ERROR: Failed to load job configurations from $JOB_CONFIG_DIR."; exit 1; }

# run hadoop streaming mapreduce job 
echo "Running data transformation routine $jobid..."
cmd="$HADOOP jar $INSTALL_ROOT/share/hadoop/tools/lib/hadoop-streaming-3.2.1.jar"
cmd="$cmd -output $WAREHOUSE_OUTPUT/$outdir"
cmd="$cmd -reducer \"/usr/local/bin/mycorona-reducer $reducers\" "
cmd="$cmd -mapper \"/usr/local/bin/mycorona-mapcsv $mappers -i $inputs\""

for input in $inputs;
do 
    cmd="$cmd -input $WAREHOUSE_INPUT/$input"
    cmd="$cmd -file $RAW_DATA_DIR/$input"
done 

echo "Command: $cmd"
eval "$cmd" || { echo "Failed to generate dataset $jobid."; exit 1; }
echo "Data transformation completed."
echo "Job output archived to warehouse directory: $outdir"

# Update the .fresh file with new output directory data
# The .fresh file should contain lines of the form:
#       idx,outdir
#       idx,outdir
#       ... 
# where idx is a reducer index, and outdir is the warehouse directory containing 
# the output from that reducer. different reducers (by id) that output to the same directory
# should simply append lines with different indices and same output directory. 
# cross referencing is handled by the warehouse data exporter
notify_fresh $jobid $outdir