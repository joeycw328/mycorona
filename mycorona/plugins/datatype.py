"""
Plugin to convert all record elements to a given type
"""

def prefetch(recordcls, cfgdata, token):
    """Get the type parameter and resolve it to a type"""
    # datatype should evaluate to an accessible type
    dtype=eval(cfgdata[token])
    return dtype

def update(record, pfdata):
    """Add all keys in keylist as attributes of record"""
    # update all record data to the specified data type
    dtype=pfdata["datatype"]
    for i in range(len(record._record)):
        try: record._record[i]=dtype(record._record[i])
        except: pass
            