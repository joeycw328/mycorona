"""
Template plugin
"""
from statistics import pstdev

TOKENS=['stddev']


class STDDevCalc(object):

    @classmethod
    def wrap(cls, record_class):
        """Add stddev calculation capabilities"""
        def _stddevfield(self):
            return self._record[stddevinfo(self).field]
        # add stddev source accessor depending on reference type 
        setattr(record_class, "_stddevfield", property(_stddevfield))
        # add function that gets standard devations from mean for this instance 
        def _stddev(self):
            # if the stddev src field contains no data, bail out
            if not self._stddevfield: return
            # figure out which stddev we want, if slicing is enabled 
            records_idx=0
            if stddevinfo(self).sliced:
                # if slicing is enabled, figure out for which slice the stddev is 
                # requested 
                records_idx=stddevinfo(self).infomap[self._active_slice]
            if not stddevinfo(self).stddevs[records_idx]:
                stddevinfo(self).stddevs[records_idx]=pstdev(stddevinfo(self).records[records_idx])
            # calculate standard deviations from mean and return 
            mean=stddevinfo(self).means[records_idx]
            stddev=stddevinfo(self).stddevs[records_idx]
            return (self._stddevfield-mean)/stddev
        setattr(record_class, "stddev", _stddev)
    

def stddevinfo(record_or_class):
    """Return the contianer class set by the mkstddevinfo function"""
    return record_or_class._stddevinfo

def mkstddevinfo(record_class, cfgdata=None):
    """Attach stddev info to the recordclass"""
    # create dynamic metadata container class
    class __stddevinfo:
        field       = 0
        means       = []
        stddevs     = []
        totals      = []
        records     = []
        sliced      = False
        infomap     = {0:0}

    # add plugin metadata container class if none exists
    if not hasattr(record_class,"_stddevinfo"):
        setattr(record_class,"_stddevinfo", __stddevinfo)
    # configure for slicing if slice plugin is detected 
    if "slices" in cfgdata:
        from .slices import sliceinfo 
        slicemap=sliceinfo(record_class).slicemap 
        stddevinfo(record_class).infomap={}
        stddevinfo(record_class).infomap.update(slicemap)
        stddevinfo(record_class).sliced=True
    # setup inital stddevinfo based on infomap data 
    for i in stddevinfo(record_class).infomap.keys():
        stddevinfo(record_class).means+=[0.0]
        stddevinfo(record_class).stddevs+=[0.0]
        stddevinfo(record_class).totals+=[0.0]
        stddevinfo(record_class).records+=[[]]

def prefetch(recordcls, cfgdata, token, pfdata):
    """Prefetch data that can be used by this/other plugins"""
    data=cfgdata[token]
    # check for field index as integer 
    try:
        field=int(data) 
    # check for field index as input from field plugin
    except ValueError:
        err=""
        if "fields" not in pfdata: 
            err="'fields' token must exist if using named field"
        elif data not in pfdata["fields"]:
            err="'%s' is not a valid field name, expected value in %s"
            err=err%(data, pfdata["fields"].keys())
        if err: raise ValueError(err)
        field=pfdata["fields"][data]
    # make sure record class has stddev info 
    mkstddevinfo(recordcls, cfgdata)
    stddevinfo(recordcls).field=field
    # wrap record class in plugin infrastructure 
    STDDevCalc.wrap(recordcls)

def update(record, pfdata):
    """Add stddev field to dataset over which stddev is calculated"""
    if stddevinfo(record).sliced:
        sources=record._slices 
    else:
        sources=[record._record]
    stddev_field_idx=stddevinfo(record).field
    for i,source in enumerate(sources):
        src_value=source[stddev_field_idx]
        if not src_value: continue
        stddevinfo(record).totals[i]+=src_value
        stddevinfo(record).records[i].append(src_value)
        new_len=len(stddevinfo(record).records[i])
        stddevinfo(record).means[i]=stddevinfo(record).totals[i]/new_len
    pass