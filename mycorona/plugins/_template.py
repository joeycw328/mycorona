"""
Template plugin
"""

TOKENS=['template']

def prefetch(recordcls, cfgdata, token):
    """Prefetch data that can be used by this/other plugins"""
    pass

def update(record, pfdata):
    """Mutate records as they are loaded into memory"""
    pass

def filter(record):
    """Filter records based on plugin policy"""
    pass