"""
Plugin to inject keys into record
"""
from .fields import add_field 


# def prefetch(recordcls, cfgdata, token, **kwargs):
#     """Prefetch data that can be used by other plugins"""
#     # add builtin fields
#     add_field(record, "maxy", -1)
#     add_field(record, "label", 0)
#     # fields should be a comma separated list of fields
#     pfdata={}
#     fields=cfgdata[token].split(',')
#     for i,field in enumerate(fields):
#         pfdata[field]=i 
#         add_field(recordcls, field, i)
#     return pfdata

def update(record, pfdata):
    """Add all keys in keylist as attributes of record"""
    # all records get the maxy field 
    for field,idx in pfdata['fields'].items():
        # try: setattr(record, field, float(record.data[idx]))
        # except: setattr(record, field, record.data[idx])
        __add_field(record, field, idx)
        record.consume(1)
            