"""
Plugin to inject keys into record
"""

def add_field(record_cls, fieldname, idx, dynamic=True):
    """
    Dynamically add a field-accessor property to the class of which record 
    is an instance
    """
    def field_proxy(self): return self._record[idx] 
    setattr(record_cls, fieldname, property(field_proxy))


def prefetch(recordcls, cfgdata, token):
    """Prefetch data that can be used by other plugins"""
    # add builtin fields
    add_field(recordcls, "maxy", -1)
    # fields should be a comma separated list of fields
    pfdata={}
    fields=cfgdata[token].split(',')
    for i,field in enumerate(fields):
        pfdata[field]=i 
        add_field(recordcls, field, i)
    recordcls.consume(len(fields))
    return pfdata

def update(record, pfdata):
    """Add all keys in keylist as attributes of record"""
    pass