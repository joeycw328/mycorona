"""
Template plugin
"""
from mycorona.web.dataset import NameStore

SLICES          = "slices"
SLICETYPES      = "slicetypes"
SLICEDELIM      = "slicedelim"
DEFAULTSLICE    = "defaultslice"


TOKENS=[
    SLICES,
    SLICETYPES,
    SLICEDELIM,
    DEFAULTSLICE
]



class SlicedRecord(object):

    @classmethod
    def inject(cls, record):
        """Add slicing capabilities to record in-place"""
        # unpack slices from record 
        slices=unpack_slices(record._record)
        # if slicetypes are configured, update types for all slice elements 
        for idx,dtype in enumerate(sliceinfo(record).slicetypes):
            dslice=slices[idx]
            for i in range(len(dslice)): 
                try: dslice[i]=dtype(dslice[i])
                # ignore type errors. allows lazy configuration for e.g. records 
                # that start with a few string headers
                except ValueError: pass 
        # add slices to record, and [s|g]et_slice function to enable/query them 
        setattr(record, "_slices", slices)
        def _set_slice(slice_id):
            try:  slice_0_indexed=sliceinfo(record).slicemap[slice_id]
            except KeyError: 
                raise ValueError("No slice with id '%s'"%str(slice_id))
            record._record=record._slices[slice_0_indexed] 
            record._active_slice=slice_id 
        def _active_slice(): return record._active_slice 
        setattr(record, "set_slice", _set_slice)
        setattr(record, "active_slice", _active_slice)
        # add a handy namestore so slices aren't referenced as strings. Note this
        # only works if slices have string names, otherwise referencing them by 
        # int index is just as easy
        _slice=NameStore()
        for slice_id in sliceinfo(record).slicemap.keys():
            try: setattr(_slice, slice_id, slice_id)
            except: break 
        # only set convenience attribute if all slices were added by name 
        else: 
            _slice.close()
            setattr(record, "slice", _slice)
        # set the slice from configured default value 
        record.set_slice(sliceinfo(record).defaultslice) 

def sliceinfo(record_or_class):
    """Return the contianer class set by the mksliceinfo function"""
    return record_or_class._sliceinfo

def mksliceinfo(record_class):
    """Attach slice info to the recordclass"""
    class __sliceinfo:
        slicemap        = {}
        slicetypes      = []
        slicedelim      = ':'
        defaultslice    = 0
    # add plugin metadata container class if none exists
    if not hasattr(record_class,"_sliceinfo"):
        setattr(record_class,"_sliceinfo", __sliceinfo)

def prefetch(recordcls, cfgdata, token):
    """Prefetch slice data"""
    data=cfgdata[token]
    # depending on the token, we'll do different things
    if token==SLICES:
        # make sure record class has slice info 
        mksliceinfo(recordcls)
        # if an integer is entered, use integers as slice ids 
        try:  
            slice_ids=range(int(data)) 
        # if it's not an integer, it should be a comma separated list of strings,
        except ValueError: 
            slice_ids=data.split(',')
        # note, there must be more than one slice 
        if len(slice_ids)<=1:
            msg="'%s' value '%s' defines %d slice(s), must be >= 2"
            msg=msg%(SLICES, data, len(slice_ids))
            raise RuntimeError(msg)
        # map slice names to array indices (see update function)
        slicemap={slice_id:idx for (idx,slice_id) in enumerate(slice_ids)}
        sliceinfo(recordcls).slicemap=slicemap
        return sliceinfo(recordcls).slicemap
    
    # set the type for elements of each slice
    elif token==SLICETYPES:
        # ensure the slices token already exists
        check_for_slices_config(cfgdata, token)
        # ensure there are types defined for each slice 
        data=data.split(',')
        slices=sliceinfo(recordcls).slicemap
        if len(slices)!=len(data):
            msg="Config defines %d %s, but %d %s found"
            msg=msg%(len(slices_cfg),SLICES,len(data),SLICETYPES)
            raise ValueError(msg)
        # store type modifier for each slice 
        slicetypes=[] 
        for stype in data: 
            try: slicetypes.append(eval(stype))
            except: 
                msg="Couldn't evaluate %s type: '%s'"
                raise ValueError(msg%(SLICETYPES,stype))
        sliceinfo(recordcls).slicetypes=slicetypes 
        return slicetypes
    
    # set the per-record slice delimiter
    elif token==SLICEDELIM:
        # ensure the slices token already exists
        check_for_slices_config(cfgdata, token)
        # store slice delimiter
        sliceinfo(recordcls).slicedelim=str(data) 
        return sliceinfo(recordcls).slicedelim
    
    # set the default slice
    elif token==DEFAULTSLICE:
        # ensure the slices token already exists
        check_for_slices_config(cfgdata, token)
        # if default slice is an integer, use it outright 
        try:
            sliceinfo(recordcls).defaultslice=int(data) 
            return sliceinfo(recordcls).defaultslice
        # otherwise it should resolve to a named slice
        except:
            if data not in sliceinfo(recordcls).slicemap:
                msg="%s value '%s' not in configured slices %s"
                msg=msg%(DEFAULTSLICE, data, sliceinfo(recordcls).slicemap.keys())
                raise ValueError(msg) 
            sliceinfo(recordcls).defaultslice=data
            return sliceinfo(recordcls).defaultslice

def update(record, pfdata):
    """Mutate records as they are loaded into memory"""
    SlicedRecord.inject(record)
    pass

# def filter(record):
#     """Filter records based on plugin policy"""
#     pass

# helper functions 
def check_for_slices_config(cfgdata, token):
    """Get config data for the 'slices' token, or raise error"""
    if SLICES not in cfgdata:
        msg="'%s' token must appear before '%s' token"
        raise RuntimeError(msg%(SLICES, token))
    return cfgdata[SLICES]


def pack_slices(*args, delimiter=':'):
    """"
    Format the inputed lists as sliced data usable with the slice plugin
    
    Arguments should be lists of data, each representing a slice of the data 
    (i.e. an interpretation of the same dataset, in e.g. linear, or log, or scaled
    etc. form). Lists should be the same length or a ValueError is raised. 

    A single list is returned, containing the data packed into usable form
    """
    # make sure more than one slice was entered
    if len(args)<=1:
        msg="Expected 2 or more slices are aguments, got %d"%len(args)
        raise ValueError(msg)
    slices=args
    # make sure all slices are the same length
    slice_len=len(slices[0])
    for idx,dslice in enumerate(slices):
        if len(dslice)==slice_len: continue 
        msg="Slice %d is len %d, expected %d"
        raise ValueError(msg%(idx,len(dslice),slice_len))
    # merged slices (as string) with delimiter
    retval=[]
    dlim=str(delimiter)
    for i in range(slice_len):
        merged=dlim.join([str(dslice[i]) for dslice in slices])
        retval.append(merged)
    
    return retval

# helper functions 
def unpack_slices(slice_data, slice_ids=[], delimiter=':'):
    """"
    Unpack the sliced data list into a list of data slices
    
    Return value will be a dictionary containg key value pairs (id,slice) where:
        id      is an integer or string identifying the slice
        slice   is a list representing a slice of the data (see pack_slices)
    
    Note that the elements of slice will be strings. Type conversion is performed
    in a different function (see type_slices)

    The function expected each element of slice_data to contain the same number of 
    slices, or only one slice. In the case that only one element is found, it is 
    duplicated among the slices. Otherwise, if an element is encountered that 
    breaks this invariance, a ValueError is raised
    """
    num_slices=0
    dlim=delimiter
    # try to find how many slices each element should contain 
    for elem in slice_data:
        num_slices=len(elem.split(dlim))
        if num_slices>1: break
    else:
        msg="Input contained <=1 slice."
        raise ValueError(msg) 
    # extract slice data 
    slices=[[] for i in range(num_slices)]
    for idx,elem in enumerate(slice_data):
        # convert delimited data to original data and check for length
        slice_elems=elem.split(dlim)
        if len(slice_elems)==1: slice_elems=slice_elems*num_slices
        elif len(slice_elems)!=num_slices:
            msg="Expected %d slices for input element %d, got %d"
            raise ValueError(msg%(num_slices,idx,len(slice_elems)))
        # add elements to each slice
        for i in range(num_slices):
            slices[i].append(slice_elems[i])

    return slices