"""
Centralized entry point for mappers and reducers 
"""

from .mapreduce import reducers

from .mapreduce.mappers import mapcsv


# entry point for csv mapper 
enter_mapcsv=mapcsv.enter

# entrypoint for all reducers
enter_reducer=reducers.enter