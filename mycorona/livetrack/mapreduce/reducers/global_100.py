
from operator import itemgetter
import sys
import threading 
from threading import Thread 
from queue import Queue 
from datetime import datetime

import math

class tracked:
    cases               = {}
    current_country     = None
    finishing_queue     = Queue()
    completed           = False

def finish_country_async(country):
    """
    Asynchronously finish data processing for the specified country
    """
    tracked.finishing_queue.put((country, tracked.cases[country]))

def _reduce_since_100_cases():
    """
    Reducer that creates the dataset of all countries' daily counts since they've 
    reached 100 total cases
    """
    cases_on_date=None
    # input comes from STDIN
    for line in sys.stdin:
        # remove leading and trailing whitespace
        line = line.strip()
        # split the key value
        key,value=line.split(';')
        # extract datapoints 
        (country,province,lat,lon)=eval(key)
        (date,total_cases)=eval(value)
        total_cases=int(total_cases)
        # in this reducer, we don't care about dates who's confirmed count is less
        # than 100 
        if total_cases<100: continue
        # we've encountered a record for a new country!
        if country!=tracked.current_country:
            # do processing for the previous country (skip first iteration)
            if tracked.current_country: 
                finish_country_async(tracked.current_country)
            # add new country to indexing; update tracking
            if country not in tracked.cases:
                tracked.cases[country]={}
            tracked.current_country=country
            # add date to country data 
            cases_on_date=tracked.cases[country]
        # add case count data for the received date
        if date not in cases_on_date:
            cases_on_date[date]=0
        cases_on_date[date]+=total_cases
    tracked.completed=True

def _handle_output(logarithmic=False):
    """
    Coroutine for formulating data for output
    """
    while True:
        # get a complete set of country data from the queue
        try: country,cases_per_date=tracked.finishing_queue.get()
        except: break
        # enumerate and sort 
        date_data=cases_per_date.items()
        to_date=datetime.strptime
        sorted_data=sorted([(to_date(i,"%m/%d/%y"),v) for (i,v) in date_data])
        # convert from raw dates to offsets from day 0 
        day0,ign=sorted_data[0]
        offset_data=[]
        expected_offset=0
        for date,count in sorted_data:
            # fill in any missing values with duplicates from the day before
            current_offset=(date-day0).days
            while expected_offset<current_offset:
                offset_data.append(offset_data[-1])
                expected_offset+=1
            # add offset data, accounting for logarithmic scale
            # Note: If logarithmic scale is chosen, we assume the data maps to the
            # function f(x)=100*e^x for some x
            # offset_data.append(str(count if not logarithmic else math.log(count/100.0)))
            offset_data.append(count if not logarithmic else math.log(count/100.0))
            expected_offset+=1
        # get score, trend data; account for log scale 
        if not logarithmic:
            # linear get's current value as score, linear increas as trend
            score=str(offset_data[-1])
            if len(offset_data)>1: 
                trend=str(offset_data[-1]-offset_data[-2])
            else: 
                trend=""
        else:
            score,trend=['','']
            # log gets 5 day average as score, or lifetime avg if less than 5 days long
            # (unless dataset is on day 0, which is ignored for math purposes) 
            rise=(offset_data[-1]-offset_data[-6 if len(offset_data)>=6 else 0])
            run=min(5, len(offset_data)-1)
            if run:
                log_slope=(math.e**float(rise/run))
                score="%f"%(100.0*(log_slope-1))
            # calculate change in delta 
            if run>=2:
                old_span=5 if len(offset_data)>=7 else len(offset_data)-2
                old_rise=(offset_data[-2]-offset_data[-2-old_span])
                old_slope=(math.e**float(old_rise/old_span))
                slope_delta=log_slope-old_slope
                trend="%f"%(100.0*slope_delta)

        # print record for this country 
        dset_str=','.join([score,trend]+[str(i) for i in offset_data])
        print("%s;%s"%(country,dset_str))


def enter_linear():
    # start the formatter/outputter process
    output_process=Thread(target=_handle_output)
    output_process.daemon=True 
    output_process.start()
    # start reading records on stdin and passing them to output thread 
    _reduce_since_100_cases()
    
def enter_logarithmic():
    # start the formatter/outputter process
    output_process=Thread(target=_handle_output, args=(True,))
    output_process.daemon=True 
    output_process.start()
    # start reading records on stdin and passing them to output thread 
    _reduce_since_100_cases()
    