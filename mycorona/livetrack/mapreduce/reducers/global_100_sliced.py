
from operator import itemgetter
import sys
import threading 
from threading import Thread 
from queue import Queue 
from datetime import datetime

import math

from mycorona.plugins.slices import pack_slices 

LINEAR          = 0
LOGARITHMIC     = 1

class tracked:
    cases               = {}
    current_country     = None
    finishing_queue     = Queue()
    completed           = False

def finish_country_async(country):
    """
    Asynchronously finish data processing for the specified country
    """
    tracked.finishing_queue.put((country, tracked.cases[country]))

def _reduce_since_100_cases():
    """
    Reducer that creates the dataset of all countries' daily counts since they've 
    reached 100 total cases. Data is sliced (see the slice plugin), with slice 
    0 containing linear data and slice 1 containing logarithmic data
    """
    cases_on_date=None
    # input comes from STDIN
    for line in sys.stdin:
        # remove leading and trailing whitespace
        line = line.strip()
        # split the key value
        key,value=line.split(';')
        # extract datapoints 
        (country,province,lat,lon)=eval(key)
        (date,total_cases)=eval(value)
        total_cases=int(total_cases)
        # in this reducer, we don't care about dates who's confirmed count is less
        # than 100 
        if total_cases<100: continue
        # we've encountered a record for a new country!
        if country!=tracked.current_country:
            # do processing for the previous country (skip first iteration)
            if tracked.current_country: 
                finish_country_async(tracked.current_country)
            # add new country to indexing; update tracking
            if country not in tracked.cases:
                tracked.cases[country]={}
            tracked.current_country=country
            # add date to country data 
            cases_on_date=tracked.cases[country]
        # add case count data for the received date
        if date not in cases_on_date:
            cases_on_date[date]=0
        cases_on_date[date]+=total_cases
    tracked.completed=True

def _handle_output():
    """
    Coroutine for formulating data for output
    """
    while True:
        # get a complete set of country data from the queue
        try: country,cases_per_date=tracked.finishing_queue.get()
        except: break
        # enumerate and sort 
        date_data=cases_per_date.items()
        to_date=datetime.strptime
        sorted_data=sorted([(to_date(i,"%m/%d/%y"),v) for (i,v) in date_data])
        # convert from raw dates to offsets from day 0 
        day0,count0=sorted_data[0]
        sliced_data=[[],[]]
        expected_offset=0
        for date,count in sorted_data:
            # fill in any missing values with duplicates from the day before
            current_offset=(date-day0).days
            while expected_offset<current_offset:
                sliced_data[LINEAR].append(sliced_data[LINEAR][-1])
                sliced_data[LOGARITHMIC].append(sliced_data[LOGARITHMIC][-1])
                expected_offset+=1
            # add offset data, accounting for logarithmic scale
            # Note: For logarithmic scale, we assume the data maps to the
            # function f(x)=100*e^x for some x
            sliced_data[LINEAR].append(count)
            sliced_data[LOGARITHMIC].append(math.log(count/count0))
            expected_offset+=1
        
        # get score, trend data for linear slice (linear get's current value as 
        # score, linear increase as trend)
        sliced_metadata=[[],[]]
        lin_slice=sliced_data[LINEAR]
        score=str(lin_slice[-1])
        if len(lin_slice)>1:  trend=str(lin_slice[-1]-lin_slice[-2])
        else:  trend=""
        sliced_metadata[LINEAR].extend([score, trend])
        
        # get score, trend data for log slice. log gets 5 day average as score, 
        # or lifetime avg if less than 5 days long (unless dataset is on day 0, 
        # which is ignored for math purposes) 
        log_slice=sliced_data[LOGARITHMIC]
        score,trend=['','']
        rise=(log_slice[-1]-log_slice[-6 if len(log_slice)>=6 else 0])
        run=min(5, len(log_slice)-1)
        if run:
            log_slope=(math.e**float(rise/run))
            score="%f"%(count0*(log_slope-1))
        # calculate change in delta 
        if run>=2:
            old_span=5 if len(log_slice)>=7 else len(log_slice)-2
            old_rise=(log_slice[-2]-log_slice[-2-old_span])
            old_slope=(math.e**float(old_rise/old_span))
            slope_delta=log_slope-old_slope
            trend="%f"%(count0*slope_delta)
        sliced_metadata[LOGARITHMIC].extend([score, trend])
        
        # consolidate data,metadata
        sliced_data[LINEAR]=sliced_metadata[LINEAR]+sliced_data[LINEAR]
        sliced_data[LOGARITHMIC]=sliced_metadata[LOGARITHMIC]+sliced_data[LOGARITHMIC]
        # pack data using slice plugin
        record_data=pack_slices(*sliced_data)
        # print record for this country 
        dset_str=','.join(record_data)
        print("%s;%s"%(country,dset_str))


def enter():
    # start the formatter/outputter process
    output_process=Thread(target=_handle_output)
    output_process.daemon=True 
    output_process.start()
    # start reading records on stdin and passing them to output thread 
    _reduce_since_100_cases()
    