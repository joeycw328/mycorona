import argparse 

# import reducer modules 

from . import global_100, global_100_sliced, us_100_sliced

parser=argparse.ArgumentParser(
    description="Entrypoint for mycorona livetrack reducers",
    epilog="[_atomsk_]"
)
parser.add_argument("id",help="Alphanumeric identifier for a reducer (see /etc/mycorona/reducers.d)")
parser.add_argument("-d","--debug", action="store_true", help="Run reducer in ptvsd debugging mode")


# list of reducers
# Note: later on this will be drawn from configuration files in 
# /etc/mycorona/reducers.d 
reducers={
    "0": global_100.enter_linear,
    "1": global_100.enter_logarithmic,
    "2": global_100_sliced.enter,
    "3": us_100_sliced.enter_county,
    "4": us_100_sliced.enter_state,
    "5": us_100_sliced.enter_county_by_pop,
    "6": us_100_sliced.enter_state_by_pop,
}

def enter():
    """
    Entrypoint for global days-since-100-cases reducer 
    """
    args=parser.parse_args()
    if args.debug: 
        try:
            import ptvsd 
            ptvsd.enable_attach(address=("0.0.0.0",8888))
            print("Waiting for debugger attach...")
            ptvsd.wait_for_attach()
        except:
            print("Error starting debugger")
    # get the reducer from command line args 
    reducer=reducers[args.id]
    reducer()
    