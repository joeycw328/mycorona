
from operator import itemgetter
import sys
import threading 
from threading import Thread 
from queue import Queue 
from datetime import datetime

import math

from mycorona.plugins.slices import pack_slices 

LINEAR          = 0
LOGARITHMIC     = 1


class tracked:
    RETURN_STATES       = False     # whether county or stade level data should be emitted
    ADJUST_POPULATION   = False     # whether data should be adjusted by population
    POP_DATA            = None      # population data from which to create adjustments (if applicable)
    state_cases         = {}
    county_cases        = {}
    current_state       = None
    current_county      = None
    finishing_queue     = Queue()
    completed           = False

def finish_county_async(state, county):
    """
    Asynchronously finish data processing for the specified county
    """
    tracked.finishing_queue.put((state, county, tracked.county_cases[county]))

def finish_state_async(state):
    """
    Asynchronously finish data processing for the specified county
    """
    tracked.finishing_queue.put((state, "", tracked.state_cases[state]))

def _reduce_county_state():
    """
    Reducer that creates the dataset of all countries' daily counts since they've 
    reached 100 total cases. Data is sliced (see the slice plugin), with slice 
    0 containing linear data and slice 1 containing logarithmic data
    """
    county_cases_on_date=None
    state_cases_on_date=None
    # input comes from STDIN
    for line in sys.stdin:
        # remove leading and trailing whitespace
        line = line.strip()
        # split the key value
        key,value=line.split(';')
        # extract datapoints 
        (state,county,lat,lon)=eval(key)
        (date,total_cases)=eval(value)
        total_cases=int(total_cases)
        
        # if we're in county tracking mode, do county tracking 
        if not tracked.RETURN_STATES:
            # in this mode, we can ignore data that's less than 100 outright 
            if total_cases<100: continue
            # we've encountered a record for a new county!
            if county!=tracked.current_county:
                # do processing for the previous county (skip first iteration)
                if tracked.current_county: 
                    finish_county_async(tracked.current_state, tracked.current_county)
                # add new county to indexing; update tracking
                if county not in tracked.county_cases:
                    tracked.county_cases[county]={}
                tracked.current_county=county
                # add date to county data 
                county_cases_on_date=tracked.county_cases[county]
            # add county data for the received date
            tracked.current_state=state
            if date not in county_cases_on_date:
                county_cases_on_date[date]=0
            county_cases_on_date[date]+=total_cases
        
        # if we're in state tracking mode, do state tracking 
        elif tracked.RETURN_STATES:     
            # we've encountered a new state! 
            if state!=tracked.current_state:
                # do processing for the previous state (skip first iteration)
                if tracked.current_state: 
                    finish_state_async(tracked.current_state)
                # add new state to indexing; update tracking
                if state not in tracked.state_cases:
                    tracked.state_cases[state]={}
                tracked.current_state=state
                # add date to state data 
                state_cases_on_date=tracked.state_cases[state]
            # add state data for the received date
            if date not in state_cases_on_date:
                state_cases_on_date[date]=0
            state_cases_on_date[date]+=total_cases
    
    tracked.completed=True

def _reduce_100(tracked_cases):
    """Remove all elements whose value is < 100 and return result"""
    # remove all dates with less than 100 in magnitude 
    # county_cases=tracked.county_cases[county]
    dates=[d for d in tracked_cases.keys()]
    for date in dates:
        if tracked_cases[date]<100: tracked_cases.pop(date)
    return tracked_cases
    
def _handle_output():
    """
    Coroutine for formulating data for output
    """
    while True:
        # get a complete set of county/state data from the queue
        try: state,county,cases_per_date=tracked.finishing_queue.get()
        except: break
        # reduce state data to 100+ count days only, (not needed for county
        # data) bail if there aren't any days
        if not county: cases_per_date=_reduce_100(cases_per_date)
        if not cases_per_date: continue 
        # format/print record
        try: 
            __handle_output( state, county, cases_per_date)
        except:
            print("Error processing record: %s"%str((state,county,cases_per_date)))
            raise

def __handle_output(state,county,cases_per_date):
    # enumerate and sort 
    date_data=cases_per_date.items()
    to_date=datetime.strptime
    sorted_data=sorted([(to_date(i,"%m/%d/%y"),v) for (i,v) in date_data])
    # adjust by population if necessary 
    if tracked.ADJUST_POPULATION:
        pop=tracked.POP_DATA[state]
        if county: pop=pop[county]
        sorted_data=[(i,float(v)/pop) for (i,v) in sorted_data]
    # convert from raw dates to offsets from day 0 
    day0,count0=sorted_data[0]
    sliced_data=[[],[]]
    expected_offset=0
    for date,count in sorted_data:
        # fill in any missing values with duplicates from the day before
        current_offset=(date-day0).days
        while expected_offset<current_offset:
            sliced_data[LINEAR].append(sliced_data[LINEAR][-1])
            sliced_data[LOGARITHMIC].append(sliced_data[LOGARITHMIC][-1])
            expected_offset+=1
        # add offset data, accounting for logarithmic scale
        # Note: For logarithmic scale, we assume the data maps to the
        # function f(x)=100*e^x for some x
        sliced_data[LINEAR].append(count)
        # sliced_data[LOGARITHMIC].append(math.log(count/count0))
        sliced_data[LOGARITHMIC].append(math.log(count))
        expected_offset+=1
    
    # get score, trend data for linear slice (linear get's current value as 
    # score, linear increase as trend)
    sliced_metadata=[[],[]]
    lin_slice=sliced_data[LINEAR]
    score=str(lin_slice[-1])
    if len(lin_slice)>1:  trend=str(lin_slice[-1]-lin_slice[-2])
    else:  trend=""
    sliced_metadata[LINEAR].extend([score, trend])
    
    # get score, trend data for log slice. log gets 5 day average as score, 
    # or lifetime avg if less than 5 days long (unless dataset is on day 0, 
    # which is ignored for math purposes) 
    log_slice=sliced_data[LOGARITHMIC]
    score,trend=['','']
    rise=(log_slice[-1]-log_slice[-6 if len(log_slice)>=6 else 0])
    run=min(5, len(log_slice)-1)
    if run:
        log_slope=(math.e**float(rise/run))
        score="%f"%(count0*(log_slope-1))
    # calculate change in delta 
    if run>=2:
        old_span=5 if len(log_slice)>=7 else len(log_slice)-2
        old_rise=(log_slice[-2]-log_slice[-2-old_span])
        old_slope=(math.e**float(old_rise/old_span))
        slope_delta=log_slope-old_slope
        trend="%f"%(count0*slope_delta)
    sliced_metadata[LOGARITHMIC].extend([score, trend])
    
    # consolidate data,metadata
    sliced_data[LINEAR]=sliced_metadata[LINEAR]+sliced_data[LINEAR]
    sliced_data[LOGARITHMIC]=sliced_metadata[LOGARITHMIC]+sliced_data[LOGARITHMIC]
    # pack data using slice plugin
    record_data=pack_slices(*sliced_data)
    # print record for this county 
    dset_str=','.join(record_data)
    key=state 
    if county: key=','.join([state, county])
    print("%s;%s"%(key,dset_str))


def enter():
    # start the formatter/outputter process
    output_process=Thread(target=_handle_output)
    output_process.daemon=True 
    output_process.start()
    # start reading records on stdin and passing them to output thread 
    _reduce_county_state()
    
def enter_county():
    tracked.RETURN_STATES=False 
    enter()

def enter_state():
    tracked.RETURN_STATES=True 
    enter()

def enter_county_by_pop():
    # load county population data 
    from .data.pops.county import POPULATIONS
    tracked.POP_DATA=POPULATIONS
    tracked.ADJUST_POPULATION=True
    tracked.RETURN_STATES=False 
    enter()

def enter_state_by_pop():
    # load state population data 
    from .data.pops.state import POPULATIONS
    tracked.POP_DATA=POPULATIONS
    tracked.ADJUST_POPULATION=True
    tracked.RETURN_STATES=True 
    enter()