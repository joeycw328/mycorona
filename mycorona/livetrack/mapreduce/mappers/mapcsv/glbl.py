"""
Mapper utilities for working with Johns Hopkins CSSE COVID-19 (JH) data in CSV form
"""

import sys 
import csv
import argparse 

header=None 

def map():
    """
    Prototype function that maps input from global dataset csv files. 
    
    JH data comes in line-based records of the form:

        [Province/State, Country/Region, Lat, Long, <Date>, <Date>, ... ],
        [<province/state>, <country/region>, <lat>, <long>, <count>, <count>, ... ],
        ...

    The first row contains keys for each column including the Date to which the
    column's data pertains. Subsequent rows include counts (of the relevant metric
    for the dataset) on the dates provided per column. 

    Naturally, given the format of the data, each updated dataset will include a 
    new column of data, an intricacy that must be accounted for.

    Data are outputed in record of the form: 

        (country, province, lat, lon);(date,count)

    Note that a semicolon is used as a key/value delimiter
    """
    # input comes from STDIN
    reader=csv.reader(sys.stdin)

    # map input from stdin 
    for data in reader:
        # skip header row
        if data[0]==header[0]: continue
        # get what will be the key
        key=(province,country,lat,lon)=data[:4]
        # post-process the province and country since apparently JH data engineers
        # are savages (smh)
        # map e.g. "Korea, South" to "South Korea"
        if ',' in province: 
            parts=[i.strip() for i in province.split(',')[::-1]]
            province=' '.join(parts)
        if ',' in country: 
            parts=[i.strip() for i in country.split(',')[::-1]]
            country=' '.join(parts)
        # now emit metrics for each date in the row
        for i in range(4, len(header)):
            (date,count)=[header[i], data[i]]
            print(';'.join([str((country,province,lat,lon)), str((date,count))]))


def enter(args):
    """
    Entrypoint for csv mapper 
    """
    global header
    # args=parser.parse_args()
    # read header row from csv file
    with open(args.input) as header_input:
        header=next(csv.reader(header_input))
    # run mapper 
    map()