"""
Mapper utilities for working with Johns Hopkins CSSE COVID-19 (JH) data in CSV form
"""

import sys 
import csv
import argparse 


class cfg:
    DATA_OFFSET=11
    header=[]
    pop=False

def map():
    """
    Prototype function that maps input from csv files containing JHU CSSE US data
    
    JH data comes in line-based records with a bunch of guff in the header:

        [UID, iso2, iso3, code3, FIPS, Admin2, Province_State, Country_Region, Lat, Long_, Combined_Key, <Date>, <Date>, ... ],
        ...

    The first row contains keys for each column including the Date to which the
    column's data pertains. Subsequent rows include counts (of the relevant metric
    for the dataset) on the dates provided per column. 

    Naturally, given the format of the data, each updated dataset will include a 
    new column of data, an intricacy that must be accounted for.

    Data are outputed in record of the form: 

        (Province_State, Admin2, lat, lon);(date,count)
    
    The Admin2 key (field 5) contains the county name, Province_State contains 
    the state name. State is emitted as the first key for sorting purposes to 
    help out the reducer. Though it is not emitted as mapper output, the iso2 field 
    (i.e. ISO 2-letter country code) is used for filtering purposes.

    Note that a semicolon is used as a key/value delimiter
    """
    # input comes from STDIN
    reader=csv.reader(sys.stdin)
    header=cfg.header
    # map input from stdin 
    for data in reader:
        # skip header row
        if data[0]==header[0]: continue
        # get data that contains what will comprise the key 
        (ign, ISO2, ign, ign, ign, county, state, ign, lat, lon)=data[:10]
        # ignore stuff that is in a territory. sorry guiz 
        if ISO2 != "US": continue 
        # ignore stuff that is "Unassigned" to a county 
        if county.startswith("Unassigned"): continue 
        # a bunch of records list "Out of " followed by state name as county. we'll 
        # want to filter these out for now 
        if county.startswith("Out of"): continue 
        # F the diamond princess 
        if not county: continue
        # create the key
        key=(state,county,lat,lon)
        # post-process the county and state since apparently JH data engineers
        # are savages (smh)
        # map e.g. "Korea, South" to "South Korea", as was the case periodically
        # in the global data 
        if ',' in county: 
            parts=[i.strip() for i in county.split(',')[::-1]]
            county=' '.join(parts)
        if ',' in state: 
            parts=[i.strip() for i in state.split(',')[::-1]]
            state=' '.join(parts)
        # now emit metrics for each date in the row
        for i in range(cfg.DATA_OFFSET, len(header)):
            (date,count)=[header[i], data[i]]
            print(';'.join([str((state,county,lat,lon)), str((date,count))]))



def enter(args):
    """
    Entrypoint for csv mapper 
    """
    # read header row from csv file
    with open(args.input) as header_input:
        cfg.header=next(csv.reader(header_input))
    # run mapper 
    map()

def enter_cases(args):
    # set index to 11, since case data doesn't contain population 
    cfg.DATA_OFFSET=11 
    enter(args)

def enter_deaths(args):
    # set index to 12, since death data does contain population (field 11)
    cfg.DATA_OFFSET=12
    enter(args)
