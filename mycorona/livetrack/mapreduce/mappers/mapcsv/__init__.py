import argparse 

# import reducer modules 

from . import glbl, us

parser=argparse.ArgumentParser(
    description="Entrypoint for mycorona livetrack mappers",
    epilog="[_atomsk_]"
)
parser.add_argument("id",help="Alphanumeric identifier for mapper (see /etc/mycorona/mappers.d)")
parser.add_argument("-d","--debug", action="store_true", help="Run mapper in ptvsd debugging mode")
parser.add_argument("-i","--input",required=True, help="Input file (for csv column headers)")


# list of reducers
# Note: later on this will be drawn from configuration files in 
# /etc/mycorona/mappers.d 
mappers={
    "0": glbl.enter,
    "1": us.enter_cases,
    "2": us.enter_deaths,
}

def enter():
    """
    Entrypoint for global days-since-100-cases reducer 
    """
    args=parser.parse_args()
    if args.debug: 
        try:
            import ptvsd 
            ptvsd.enable_attach(address=("0.0.0.0",8888))
            print("Waiting for debugger attach...")
            ptvsd.wait_for_attach()
        except:
            print("Error starting debugger")
    # get the reducer from command line args 
    mapper=mappers[args.id]
    mapper(args)
    