import logging
from multiprocessing import log_to_stderr


# create some shared logging stuff
log=log_to_stderr()
# log.setLevel(logging.INFO)


import os 
from os.path import join,abspath,dirname
from .dataset import NameStore, CONFDIR

RES_DIR=abspath(join(dirname(__file__),"res"))

HTML_DIR="/var/www/html/mycorona"

ROOTPATH=HTML_DIR

IMG_DIRNAME="img"

IMG_DIR="%s/img"%HTML_DIR

DATE_FORMAT="%d-%b-%Y %H:%M:%S UTC"


# store dataset information
datasets=NameStore()

# global cases and deaths data, sliced into lin/log pieces
datasets.glbl.deaths.sliced.id              = 0
datasets.glbl.cases.sliced.id               = 1

# us county and state level case data, sliced into lin/log pieces
datasets.us.county.cases.sliced.id          = 2
datasets.us.state.cases.sliced.id           = 3
# us county and state level death data, sliced into lin/log pieces
datasets.us.county.deaths.sliced.id         = 4
datasets.us.state.deaths.sliced.id          = 5

# us county and state level case data, sliced into lin/log pieces (population biased)
datasets.uspop.county.cases.sliced.id       = 6
datasets.uspop.state.cases.sliced.id        = 7
# us county and state level death data, sliced into lin/log pieces (population biased)
datasets.uspop.county.deaths.sliced.id      = 8
datasets.uspop.state.deaths.sliced.id       = 9

# close dataset to prevent typos
datasets.close()
