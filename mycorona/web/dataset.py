"""
Utilities for loading/storing dataset information
"""
import argparse 
from configparser import ConfigParser
import pathlib 

from . import log

CONFDIR="/etc/mycorona/conf.d"

class DatasetConfigError(Exception):
    """Exception for errors that occur during dataset configuration"""
    def __init__(self, msg, dataset_id=None):
        entity="metadata" if dataset_id==None else "dataset %s"%str(dataset_id)
        err_str="Error loading dataset %s: "%entity
        super(DatasetConfigError, self).__init__(err_str+msg)

class NameStore(argparse.Namespace):
    """
    Namespace with lockable mutability
    """
    def __init__(self, *args, **kwargs):
        self.__closed=False
        self.__subspaces=[]
        super(NameStore, self).__init__(*args, **kwargs)

    def __getattr__(self, key):
        """Override to provide locking"""
        try:
            return self.__getattribute__(key)
        except AttributeError:
            # if locked, and no attribute, raise 
            if self.__closed: 
                raise 
        # otherwise create a new namespace and return it 
        ns=NameStore()
        self.__subspaces.append(ns)
        object.__setattr__(self,key,ns)
        return ns
    
    def __setattr__(self, key, value):
        """Override that supports closing"""
        if "__" in key:
            return object.__setattr__(self,key,value) 
        if self.__closed:
            if not hasattr(self, key):
                err="NameStore is closed and has no attribute '%s'"%str(key)
                raise AttributeError(err)
        try: 
            val=self.__getattribute__(key)
            if isinstance(val, NameStore):
                err="Cannot overwrite existing substore %s"%key 
                raise AttributeError(err)
        except: pass
        return super(NameStore,self).__setattr__(key,value)
    def close(self):
        self.__closed=True
        for ns in self.__subspaces: ns.close()

class MetadataLoader(object):
    
    def __init__(self, cfgdir=CONFDIR):
        self.__parser=None 
        self.__parsed_paths=set()
        self.__cfgdir=cfgdir

    def load_metadata(self):
        """ Parse configdir and store discovered dataset metadata """
        # check for config dir existence
        cfgpath=pathlib.Path(self.__cfgdir)
        if not cfgpath.exists():
            msg="config dir '%s' doesn't exist"%self.__cfgdir
            raise DatasetConfigError(msg)
        if not cfgpath.is_dir():
            msg="config path '%s' is not a directory"%self.__cfgdir
            raise DatasetConfigError(msg)
        # get configfile paths
        configs=cfgpath.glob("*")
        if not configs:
            msg="config dir '%s' is empty"%self.__cfgdir 
            raise DatasetConfigError(msg)
        # load config files into config parser
        if self.__parser==None: self.__parser=ConfigParser()
        parser=self.__parser 
        parsed_paths=self.__parsed_paths
        try:
            if self.__cfgdir in parsed_paths: pass 
            else:
                # parse configs
                parser.read(configs)
                parsed_paths.add(self.__cfgdir)
        except Exception as e: 
            raise DatasetConfigError(str(e))
        log.info("Loaded configfile")


    def get_metadata(self, dataset_id):
        """Get section with the stated id"""
        if not self.__parser:
            msg="Must call load_metadata before calling get_metadata"
            raise RuntimeError(msg) 
        try:
            return self.__parser[str(dataset_id)]
        except Exception as e: 
            raise DatasetConfigError(str(e), dataset_id)