"""
Basic implementation of a plotted figure. 
"""
import pathlib 
from . import util
from mycorona.web.plugin.manager import PluginManager

from . import log

_DANGER_LINES=[
    (1.2599,"26",3),
    (1.189207,"19",4)
]

class Figure(PluginManager):
    """
    Basic implementation of a figure. 
    """
    # figure render related attributes (user overridable)
    xlabel      = "x-axis"
    ylabel      = "y-axis"
    title       = "basic plot"
    filename    = "default.png"
    minlen      = 0
    
    # attributes for internal use by the mycorona system (user overridable)
    installdir  = ""

    # attributes used internally by the Figure itself
    __registry  = []

    DANGER_LINES=_DANGER_LINES

    def __init__(self, datastore, logarithmic=False):
        """
        Global outlook figure with option to produce figure in logarithmic scale
        """
        super(Figure, self).__init__()
        self.__registry.append(self)
        self.__idx=len(self.__registry)-1
        self.__arranged=False
        self.__logarithmic=logarithmic
        # get datastore by 'id' attribute, otherwise by (legacy) 'logarithmic' 
        # and 'linear' attributes
        try: self.__dataset=datastore.id
        except AttributeError: 
            self.__dataset=datastore.logarithmic if logarithmic else datastore.linear
        self.__danger_max_height=True
        log_string="Logarithmic" if logarithmic else "Linear"
        for key in ["xlabel","ylabel","title"]:
            value=getattr(self, key)
            try: setattr(self, key, value%log_string)
            except: pass 
        log_string="_logarithmic" if logarithmic else ""
        try: self.filename=self.filename%(log_string)
        except: pass
        self.__maxy=0
        self.__longest_record=0
        self.__records=[]
        self.__records_loaded=False

    @property 
    def maxy(self): return self.__maxy 
    @property 
    def dlen(self): return self.__longest_record

    def arranged(self):
        """
        This function help to ensure figures are added to the webpage in the order
        they are created. Since they are added in the order they are discovered 
        in the directory, this function prepends the filename with an index that 
        will help ensure proper sort order.
        """
        if not self.__arranged:
            mypath=pathlib.Path(self.filename)
            parent=mypath.parent
            name=("%02d_"%self.__idx)+str(mypath.name)
            self.filename=str(parent/name)
            self.__arranged=True 
        return self


    @property 
    def is_logarithmic(self): return self.__logarithmic
    
    @property 
    def has_max_danger_height(self): return self.__danger_max_height
    
    @property 
    def dataset(self):
        """
        Return the dataset passed to the constructor. 

        The function prefers the datastore passed to the constructor, but will 
        fall back to the hard-coded dataset parameter
        """
        return self.__dataset

    def with_danger_max(self):
        """
        Set danger_max to True. Returns this instance for chaining, i.e.:
        
            myfig=Figure(0)
            myfiglog=Figure(0).with_danger_max()
        """
        self.__danger_max_height=True 
        return self

    def without_danger_max(self):
        """
        Set danger_max to False. Returns this instance for chaining, i.e.:
        
            myfig=Figure(0)
            myfiglog=Figure(0).without_danger_max()
        """
        self.__danger_max_height=False 
        return self

    @classmethod 
    def registry(cls):
        """
        Return the list of registered figures
        """
        return cls.__registry


    def xticks(self, longest_record):
        """
        Return tuple containing x-axis tick and label frequency

        For example, returning (1,1) would create a ticks with increments of 1 on,
        the x-axis, and every tick would be labeled. Returning (1,3) would create 
        ticks with increments of 1, but only every 3rd tick would be labeled. 

        Defaults to (1,1)
        """
        return (1,1)

    def load_data(self):
        """"
        Load and store all records 
        """
        # if records alread loaded, do nothing
        if self.__records_loaded: return 
        # bootstrap the plugin manager
        record_class=PluginManager.bootstrap(self, self.dataset)
        # load records from raw data, allowing plugins to mutate them
        # each loaded dataset get it's own type, with unique id, to allow plugins 
        # add properties without stepping on other loads 
        log.info("Updating records from plugins...")
        for record in util.get_data(self.dataset, record_class):
            log.debug("Updating record: %s"%str(record))
            PluginManager.update(self, record)
            self.__records.append(record)
        # run record through filters and store resulting dataset 
        log.info("Record update complete.")
        log.info("Compiling dataset...")
        self.__compile()
        log.info("Dataset compiled.")
        self.__records_loaded=True        


    def __compile(self):
        """Run records through filtering routine(s) and store result/metadata for quick retrieval"""
        # filter through plugins 
        log.info("Performing plugin filters...")
        pm_filtered=[PluginManager.filter(self, i) for i in self.__records]
        log.info("Plugin filters completed.")
        # filter through derived class implementation; store
        log.info("Performing implementation filters...")
        records=[self.filter(i) for i in pm_filtered]
        log.info("Implementation filters completed.")
        # get some metadata that are expected by the plot renderer 
        longest_record=0
        high_score=0
        self.__records=[]
        for record in records: 
            if not record:  continue
            log.info("Postprocessed: %s"%str(record.label))
            self.__records.append(record)
            if record.dlen > longest_record: longest_record=record.dlen
            if record.maxy > high_score: high_score=record.maxy
        self.__maxy=high_score 
        self.__longest_record=longest_record


    def get_records(self):
        """Return a list of records filtered using this figure's filter implementation"""
        # ensure records have been loaded
        if not self.__records_loaded:
            msg="Must call load_data before calling get_records"
            raise RuntimeError(msg)
        # return record set (sliced, so as not to be mutable)
        return self.__records[:]

    
    # abstract method, must be implemented
    def filter(self, record):
        """
        Called on all records to widdle down to those used by this figure
        """
        raise NotImplementedError("Must implement Figure.filter function")

    