# # import csv 
# import pathlib 
# import os
# from os.path import join, dirname, basename, isfile
# import glob
# import time
# # import all modules in current directory
# packages = glob.glob(join(dirname(__file__), "*/__init__.py"))
# __all__ = [ basename(dirname(p)) for p in packages]
# __all__ = [ package for package in __all__ if not package.startswith("_")]

# from mycorona import web
from mycorona.web import log
# from .base import Figure
# from . import util
# from . import *

# from multiprocessing.pool import Pool

# def _create(figure, production=True, force=False):
#     """
#     Generate dataset using figure's filter, and generate a plotted figure with 
#     the axes and title described by the figure
#     """
#     # load the figure's dataset 
#     figure.load_data()
#     # figure out when the dataset was last updated 
#     dt=util.get_dataset_date(figure.dataset)
#     dt_string=dt.strftime(web.DATE_FORMAT)
#     # pass the figure to the plotting routine
#     try:
#         log.info("Rendering figure: %s"%figure.filename )
#         return util.make_plot(figure, dt_string, production, force)
#     except:
#         print("ERROR: Generating figure: %s"%figure.filename)
#         raise


# def _cleanup(expected_figures):
#     """
#     Delete any unknown figures from the figure directory
#     """
#     webroot=pathlib.Path(web.ROOTPATH)
#     imgdirs=[i for i in webroot.glob('*/img/*')]
#     imgdirs+=[i for i in webroot.glob('*/*/img/*')]
#     for subdir in imgdirs:
#         extant_figs=subdir.glob('*')
#         for fig in extant_figs:
#             if fig.name not in expected_figures:
#                 print("Cleaning up unused figure: %s"%fig)
#                 fig.unlink()

# def make_figures(args):
#     """
#     Create all figures and place them in the served data directory
#     """
#     # keep track of which figures were created to assist in cleanup 
#     built_figures=set()
#     fresh_datasets=set()
#     async_results=[]
#     num_renders=0
#     # create a process pool to accelerate rendering 
#     res_pool=Pool(args.workers)
#     # get figures from the registry and toss them to the resource pool
#     figures=Figure.registry()
#     production=not args.debug
#     start=time.time()
#     for figure in figures:
#         async_results.append(res_pool.apply_async(_create, (figure,production, args.force)))
#     res_pool.close()
#     res_pool.join()
#     # wait for all processes to complete, add store paths to figures 
#     for result in async_results:
#         dset,fig_path=result.get()
#         if fig_path: 
#             built_figures.add(fig_path.name)
#             print("Completed figure: %s"%str(fig_path))
#         if dset!=None: 
#             fresh_datasets.add(dset)
#             num_renders+=1
#     end=time.time()-start
#     print("Rendered %d figures in %.2f seconds"%(num_renders,end))
#     # cleanup any old/unrecognized figures in the figure directory 
#     if not args.debug:
#         _cleanup(built_figures)
#     fresh_datasets=sorted(list(fresh_datasets))
#     print("Datasets containing new data: %s"%str(fresh_datasets))
#     if fresh_datasets: util.update_known_hashes(fresh_datasets)
#     else: print("No datasets contain new data! Skipping hash update.")
