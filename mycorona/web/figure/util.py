"""
Utilities for creating plot figures
"""
import matplotlib
import matplotlib.pyplot as plt
from os.path import join, dirname, basename, isfile
import math 
from adjustText import adjust_text
import sys
import csv 
import pathlib 
from collections import OrderedDict
from datetime import datetime 
import random 
import math 
import shutil

from mycorona.web import ROOTPATH, IMG_DIRNAME

# input dir for raw datasets
DATA_DIR="/var/lib/mycorona/data/transform"
DATASET="dataset.%d.dat"

# output dir for generated figures
# FIGURE_DIR=pathlib.Path(ROOTPATH)

# growth rate that will be referenced in figures
# DANGEROUS_GROWTH_RATE=1.3195
DANGEROUS_GROWTH_RATE=1.2599

# # UNHEALTHY_GROWTH_RATE=1.2599
UNHEALTHY_GROWTH_RATE=1.189207


# format for timestamping 
TSTAMP_FORMAT="%Y%m%d.%H%M%S"

# figure characteristics
LINE_WIDTH          = 1
LABEL_SIZE          = 3
TIMESTAMP_LOCATION  = (12,12)
DISCLAIMER_LOCATION = (1000,12)


class _cache:
    FRESH_DATASETS      = set()
    STALE_DATASETS      = set()

def danger_line(num_days, figure=None, initial_confirmed=100.0, max_height=None, rate=DANGEROUS_GROWTH_RATE):
    """
    Calculate a line that's 33% growth for the duration of the line
    """
    ret=[]
    for i in range(num_days):
        danger_count=initial_confirmed*((rate)**(i))
        if figure and figure.is_logarithmic: 
            danger_count=math.log(danger_count/initial_confirmed)
        ret.append(danger_count)
        if max_height and danger_count > max_height: break 
    # add hazard line 
    return ret



def strpid(dataset_id):
    """
    Get path to dataset from id 
    """
    # open the dataset and load into csv reader
    dataset=DATASET%dataset_id 
    data_path=pathlib.Path(join(DATA_DIR, dataset))
    if not data_path.exists():
        msg="Dataset %d expected at path %s"%(dataset_id, data_path)
        raise ValueError(msg)
    return data_path 

def get_data(dataset_id, record_class):
    """
    Return list of records in the dataset with the given id     
    """ 
    # open the dataset and load into csv reader
    data_path=strpid(dataset_id)
    with open(data_path) as raw_data:
        for record in csv.reader(raw_data): 
            yield record_class(record)

def current_hashfile(dataset_id):
    """Convert dataset id into current (latest) hashfile for the dataset (i.e. <dataset>.sha256"""
    dataset=DATASET%dataset_id 
    data_path=pathlib.Path(join(DATA_DIR, dataset))
    # convert dataset name to current hashname 
    current_hashfile='.'.join([data_path.name,"sha256"])
    return data_path.parent/current_hashfile
    
def known_hashfile(dataset_id):
    """Convert dataset id into last known hashfile for the dataset (i.e. <dataset>.sha256.known"""
    # convert dataset name to current hashname 
    latest=current_hashfile(dataset_id)
    # convert current name to last known name
    return latest.with_suffix(".sha256.known")
    
def is_dataset_fresh(dataset_id):
    """Check if the dataset is fresh(by hash) and return true or false"""
    # convert dataset name to current hashname 
    print("Checking freshness of dataset: %s"%str(dataset_id))
    if dataset_id in _cache.FRESH_DATASETS:
        print("Cached result says dataset contains fresh data!")
        return True 
    elif dataset_id in _cache.STALE_DATASETS:
        print("Cached result says dataset is contains no fresh data!")
        return False 
    latest=current_hashfile(dataset_id)
    # convert current hashname to known hashname 
    last_known=known_hashfile(dataset_id)
    # if last known hashfile doesn't exists, return false 
    if not last_known.exists():
        print("Hashfile %s doesn't exist. Considering dataset fresh"%str(last_known))
        _cache.FRESH_DATASETS.add(dataset_id)
        return True 
    # check if last known hash is equal to current hash 
    with open(str(latest)) as latest_hashfile:
        latest=latest_hashfile.read().strip()
    with open(str(last_known)) as known_file:
        last_known=known_file.read().strip()
    print("Latest hash: \n  %s"%str(latest))
    print("Last known hash: \n  %s"%str(last_known))
    fresh=latest!=last_known
    if fresh: _cache.FRESH_DATASETS.add(dataset_id)
    else: _cache.STALE_DATASETS.add(dataset_id)
    print("Cached result: %s"%(str((dataset_id,fresh))))
    return fresh
    
def update_known_hashes(iter_dataset_ids):
    """Update known hashfiles to values from current hashfiles"""
    # iterate through the dataset ids and update all of their hashes 
    for dset_id in iter_dataset_ids:
        # get the current, and known hashfiles
        latest_hash=current_hashfile(dset_id)
        known_hash=known_hashfile(dset_id)
        # copy latest to known 
        print("Updating hash %s"%str(known_hash))
        shutil.copy(str(latest_hash), str(known_hash))

def get_dataset_date(dataset_id):
    """
    Return the date/time that the dataset was generated as a datetime object
    """
    data_path=strpid(0)
    linkend=data_path.resolve(strict=True).name 
    # extract date from filename 
    datestr='.'.join(str(linkend).split('.')[:-2])
    return datetime.strptime(datestr, TSTAMP_FORMAT)


def _make_ticks(length, tick_freq, label_freq, start=0):
    """
    Return list of ticks and labels 
    """
    xticks=range(start,length,tick_freq)
    labels=[""]*len(xticks)
    # for tick in xticks:
    for i in range(0, len(xticks), label_freq):
        labels[i]=xticks[i]
    return xticks,labels

# def make_plot(record_list, figure, date, production=True):
def make_plot(figure, date, production=True, force=False):
    """
    Plot data including danger line, labels, titles, etc.

    Returns path to newly created figure, as pathlib.Path object
    """
    # if the figure's dataset has no new data, no need to regenerate it 
    if force:
        print("Skipping freshness check for figure: %s"%figure.filename)
    elif not is_dataset_fresh(figure.dataset):
        msg="Figure %s is based on dataset %d, which contains no new data. Skipping render."
        print(msg%(figure.filename, figure.dataset))
        return None, pathlib.Path(figure.filename)
    
    # get figure plot data 
    record_list=figure.get_records()
    # if figure has no records, skip it 
    if not figure.dlen: return None,None
    # get figure metadata
    xlabel=figure.xlabel
    ylabel=figure.ylabel
    title=figure.title
    filename=figure.filename

    # create matplotib plot
    fig, ax = plt.subplots(dpi=500)
    label_texts=[]

    # add danger line 
    longest_record=figure.dlen
    high_score=figure.maxy
    max_height=high_score if figure.has_max_danger_height else None
    # allow figure to specify a minimum length, which we enforce by creating the 
    # danger line(s) with that length
    danger_lines=[]
    if figure.minlen: longest_record=max(longest_record, figure.minlen)
    if not figure.DANGER_LINES:
        pass 
    else:
        y_data=danger_line(
            longest_record, max_height=max_height, 
            figure=figure
        )
        dline=ax.plot(y_data, "k--", linewidth=LINE_WIDTH)
        txt_32=ax.text(len(y_data)-1, y_data[-1], "+26% Daily", fontsize=LABEL_SIZE+1, fontweight='bold')
        label_texts.append(txt_32)
        danger_lines.append((dline[0],26, 3))
    # danger_lines.append((txt_32,32, 2.5))
    # add hazard line to logarithmic figures
    if figure.is_logarithmic:
        if not figure.DANGER_LINES:
            pass
        else:
            y_data=danger_line(
                longest_record, max_height=max_height, 
                figure=figure, rate=UNHEALTHY_GROWTH_RATE
            )
            dline=ax.plot(y_data, "k--", linewidth=LINE_WIDTH)
            txt_26=ax.text(len(y_data)-1, y_data[-1], "+19% Daily", fontsize=LABEL_SIZE+1, fontweight='bold')
            label_texts.append(txt_26)
            danger_lines.append((dline[0],19, 4))
        # danger_lines.append((txt_26,26, 3))
    # add country data
    record_list=sorted(record_list, reverse=True, key=lambda record: record.dlen)
    for record in record_list:
        lbl,y_data=record.label, record.data
        ax.plot(y_data, linewidth=LINE_WIDTH, marker='.', markersize=LINE_WIDTH+1)
        if figure.is_logarithmic:
            # add avg rate, percent, and daily change in rate
            score='-'
            if record.score: score="%.2f%%"%(record.score)
            # calculate change in delta 
            if record.trend!="": score+=" (%+.1f%%)"%(record.trend)
            lbl="%s %s"%(lbl, score)
        else:
            # add total cases, absolute change in cases
            score="%s"%format(int(record.score),",")
            if record.trend!="": score+=" (+%s)"%format(int(record.trend),",")
            lbl="%s %s"%(lbl, score)
        new_text=ax.text(
            record.dlen-1, record.maxy, lbl, 
            fontsize=LABEL_SIZE, fontweight='bold'
        )
        label_texts.append(new_text)
    # set axis ticks
    ticks,labels=_make_ticks(longest_record, *figure.xticks(longest_record))
    ax.set_xticks(ticks)
    ax.set_xticklabels(labels)
    # make sure to scale the y-axis labels if the figure is logarithmic 
    if figure.is_logarithmic:
        tick=0; yticks=[]
        while True:
            next_tick=math.log(10.0**tick)
            if next_tick > high_score: break
            yticks.append(next_tick)
            tick +=1
        ax.set_yticks(yticks)
        ytick_labels=[100.0*(math.e**i) for i in yticks]
        # round for aesthetics 
        ax.set_yticklabels([str(int(round(i))) for i in ytick_labels])
    # set axis labels
    ax.set(xlabel=xlabel, title=title)
    ylbl_obj=ax.set_ylabel(ylabel)
    ax.grid()
    # add date information 
    if date:
        msg="Updated: %s"%date  
        ax.annotate(
            msg, TIMESTAMP_LOCATION, 
            fontsize=LABEL_SIZE, xycoords="figure pixels"
        )
    # disclaim that rates are 5 day averages
    plt.tight_layout()
    if figure.is_logarithmic:
        msg="Daily growth percentages\nreflect 5-day moving averages"
        disclaimer=ax.annotate(
            msg, DISCLAIMER_LOCATION, 
            fontsize=LABEL_SIZE, xycoords="figure pixels"
        )
        dsc_width=disclaimer.get_window_extent(fig.canvas.get_renderer()).width 
        dsc_x=fig.bbox.width-(dsc_width+50) 
        disclaimer.set_position((dsc_x, 12))
    # add provenance
    msg="https://verse.1337.cx/mmmmycorona"
    provenance=ax.annotate(
        msg, (0,0), 
        fontsize=LABEL_SIZE, xycoords="figure pixels"
    )
    prov_width=provenance.get_window_extent(fig.canvas.get_renderer()).width
    offset=fig.bbox.width; pad=12
    if figure.is_logarithmic: 
        offset/=2.0; pad=prov_width/-2.0
    prov_x=(offset)-(prov_width+pad) 
    provenance.set_position((prov_x, 12))
    # add legend 
    if figure.is_logarithmic:
        artists=[];leg_texts=[]
        for dl,percent,days in danger_lines:
            artists.append(dl)
            label="%d%%: Count doubles\nevery ~%s days"%(percent,str(days))
            # dl.set_label(label)
            # ax.legend(handles=[dl])
            leg_texts.append(label)
        if artists:
            ax.legend(artists, leg_texts, loc=2, fontsize=LABEL_SIZE*1.5)
    # reshuffle country labels locations to avoid collisions
    print("Adjusting figure labels: %s ..."%filename); sys.stdout.flush()
    # adjust_text(label_texts,only_move={"points":"", "text":"y"})
    adjust_text(label_texts,only_move={"points":"","text":"xy"})
    print("Adjust completed figure: %s ..."%filename); sys.stdout.flush()
    # ensure output directory exists, and write figure
    fig_path=pathlib.Path(ROOTPATH)
    if figure.installdir:
        fig_path=fig_path/figure.installdir
    fig_path=fig_path/IMG_DIRNAME/filename
    # fig_path=pathlib.Path(join(FIGURE_DIR,filename))
    print("Generating figure: %s ..."%str(fig_path)); sys.stdout.flush()
    if production: fig_path.parent.mkdir(parents=True, exist_ok=True)
    else: fig_path=pathlib.Path(fig_path.name)
    fig.savefig(fig_path)
    print("Render completed: %s ..."%str(fig_path)); sys.stdout.flush()
    plt.close(fig)
    return figure.dataset, fig_path

