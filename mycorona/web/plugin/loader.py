"""
Helper mix-in to load and validate plugins
"""
from . import log

class Plugin(object):
    """Abstraction of a plugin, wrapping a pluginapi module"""
    def __init__(self, plugin_module):
        self.__plugin=plugin_module 
        # the TOKEN is the key (i.e. left side of the equals sign) to which this 
        # plugin corresponds. If the module contains no TOKEN attribute, the 
        # module name (as in module.__name__) is used 
        try: self.__tokens=self.__plugin.TOKENS
        except: self.__tokens=[self.__plugin.__name__.split('.')[-1]]
        if not isinstance(self.__tokens, list):
            msg="Expected %s.TOKENS attribute with type list, got %s"
            mytype=type(self).__name__ 
            tokenstype=type(self.__tokens).__name__
            raise TypeError(msg%(mytype, tokenstype))
        # plugins must define the update method, even if it does nothing.
        try: self.__update=self.__plugin.update 
        except AttributeError: 
            msg="Plugin %s must implement the 'update' function"
            raise NotImplementedError(msg%str(self.__plugin))

    @property 
    def tokens(self): return self.__tokens[:]

    def prefetch(self, recordcls, cfgdata, token, pf_data):
        try: _prefetch=self.__plugin.prefetch
        except AttributeError: return 
        # try to call the prefetch function with pf_data
        try: return _prefetch(recordcls, cfgdata, token, pf_data) 
        # also allow plugin devs to ignore prefetch data 
        except TypeError: return _prefetch(recordcls, cfgdata, token) 
    
    def update(self, record, prefetchdata):
        return self.__update(record, prefetchdata)
    
    def filter(self, record):
        try: _filter=self.__plugin.filter 
        except AttributeError: return record 
        return _filter(record)

class PluginLoader(object):
    def __init__(self):
        self.__plugins={}
        self.__loaded=False 
    
    def get_plugin_for_token(self, token):
        """Return the plugin corresponding to the inputed token"""
        return self.__plugins[token]

    def is_plugin_token(self,token):
        return token in self.__plugins

    def load_plugins(self):
        """Load/validate/store plugins for later use"""
        # get all plugins from plugin dropin module 
        if self.__loaded: return 
        from mycorona import plugins 
        all_plugin_modules=plugins.get() 
        for module in all_plugin_modules:
            self.__validate(module)
            new_plugin=Plugin(module)
            log.info("Loaded plugin: %s"%str(module.__name__))
            for token in new_plugin.tokens:
                self.__plugins[token]=new_plugin
        self.__loaded=True

    def __validate(self, plugin_module):
        """Ensure the inputed module is suitable for use as a plugin"""
        # empty for now
        return True