from collections import OrderedDict
import random 

from mycorona.web.dataset import MetadataLoader
from .loader import PluginLoader

from . import log

class _EntityRecord(object):
    """
    Wrapper to provide convenient accesors
    """
    _DATA_OFFSET=0
    def __init__(self,record,dataset=None):
        self._record=record[:]
        self._data_offset=self._DATA_OFFSET
        self.label=self._record[0]
    
    @property 
    def data(self): return self._record[self._data_offset:]
    
    @property 
    def dlen(self): return len(self._record)-self._data_offset

class PluginManager(PluginLoader, MetadataLoader):
    def __init__(self):
        self.__needed_plugins=OrderedDict([])
        self.__needed_plugin_list=[]
        self.__prefetch_data={}
        self.__bootstrapped=False
        PluginLoader.__init__(self)
        MetadataLoader.__init__(self)

    class _RecordUpdateProxy(object):
        """Helper class for creating/tracking plugin-specific record metadata"""
        @classmethod 
        def wrap(cls, record, _cls=False):
            """Add some tools to the record that are useful to plugins"""
            setattr(record,"_consumed",0)    
            def _consume(num_fields): record._consumed+=num_fields
            setattr(record, "consume", _consume)
            setattr(record, "_cls", _cls)
            return record
        
        @classmethod 
        def unwrap(cls, record):
            """Perform the tasks prescribed by the record update"""
            # if fields were consumed, remove them from record data 
            _consumed=record._consumed
            if not _consumed: pass
            else: 
                if record._cls: record._DATA_OFFSET=_consumed 
                else: record._data_offset+=_consumed
            del record._consumed
            del record.consume
            del record._cls

    def __check_bootstrapped(self, err_method):
        if not self.__bootstrapped:
            msg="Must call bootstrap before calling %s"%str(err_method)
            raise RuntimeError(msg) 

    def update(self, record):
        """Run all plugin interface 'update' functions on record""" 
        # ensure we're good to go
        self.__check_bootstrapped("update")
        # add a bit of metadata to the record to help track update stuff
        self._RecordUpdateProxy.wrap(record)
        # perform update using all plugins
        for plugin in self.__needed_plugin_list:
            plugin.update(record, self.__prefetch_data)
        # teardown the metadata tracking 
        self._RecordUpdateProxy.unwrap(record)


    def filter(self, record):
        """Run all plugin interface 'filter' functions on record"""      
        self.__check_bootstrapped("filter")
        for plugin in self.__needed_plugin_list:
            if not plugin.filter(record): return None
        return record

    
    def bootstrap(self, dataset_id):
        """Load plugins, load config data, and call all prefetch functions"""
        # load plugins, metadata
        log.info("Bootstrapping dataset id: %d"%dataset_id)
        self.load_plugins(); self.load_metadata()
        # get the plugins needed to parse the metadata for this dataset
        # Note that since the configparser uses an ordereddict underneath, the 
        # plugins are added in the order they appear in the config file, which 
        # is intended behavior 
        class EntityRecord(_EntityRecord):
            GROUPID="%#x"%random.getrandbits(64)
        dataset_metadata=self.get_metadata(dataset_id)
        md_tokens=dataset_metadata.keys()
        already_needed=set()
        for token in md_tokens:
            if not self.is_plugin_token(token): continue 
            plugin=self.get_plugin_for_token(token)
            self.__needed_plugins[token]=plugin
            if id(plugin) in already_needed: continue 
            # also build a list for quick sequential access
            self.__needed_plugin_list.append(plugin)
            already_needed.add(id(plugin))
        # add a bit of metadata to the record class to help track prefetch stuff
        log.info("Config wants plugins: %s"%self.__needed_plugins.keys())
        record=EntityRecord 
        log.info("Running prefetch...")
        self._RecordUpdateProxy.wrap(record, _cls=True)
        # prefetch data from all plugins
        for token,plugin in self.__needed_plugins.items():
            log.info("Prefetching token: %s"%token)
            pf_data=plugin.prefetch(EntityRecord, dataset_metadata, token, self.__prefetch_data)
            if pf_data: self.__prefetch_data[token]=pf_data
        # teardown the metadata tracking 
        self._RecordUpdateProxy.unwrap(record)
        # # add default plugin data 
        # for token,pf_data in _PLUGIN_DEFAULTS:
        #     if token not in self.__prefetch_data:
        #         self.__prefetch_data[token]={}
        #     self.__prefetch_data[token].update(pf_data)
        self.__bootstrapped=True
        log.info("Bootstrap complete")
        return EntityRecord
        

