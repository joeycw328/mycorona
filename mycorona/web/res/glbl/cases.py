from twisted.web.template import Element, renderer, XMLFile, tags
from twisted.python.filepath import FilePath
from os.path import join
from datetime import datetime
import pathlib 

from mycorona.web.res.base import UberTabGlobal 
from . import TEMPLATEDIR

class CasesElement(UberTabGlobal, Element):
    
    TEMPLATE_FILE=join(TEMPLATEDIR,'cases.xml')
    
    loader = XMLFile(FilePath(TEMPLATE_FILE))
    def __init__(self, *args, **kwargs):
        """Init tabname and href"""
        UberTabGlobal.__init__(self, "Confirmed Cases", "cases.html")
        Element.__init__(self, *args, **kwargs)

    

    def get_outlook(self, request, tag, outlook):
        """
        Add all images in the specified dir to the html document
        """
        doc_rootpath=self.fs_root
        global_imgdir=doc_rootpath/"img"/outlook
        # add all images in directory, each wrapped in an href reference to the 
        # full picture
        for png in sorted(global_imgdir.glob("*cases*.*")):
            img_src=str(png.relative_to(doc_rootpath))
            tag(tags.a(href=img_src)(tags.img(src=img_src, alt=png.name)))
        return tag

    
    @renderer
    def outlook_global(self, request, tag):
        return self.get_outlook(request, tag, "global")

    @renderer
    def outlook_developing(self, request, tag):
        return self.get_outlook(request, tag, "developing")
    
    @renderer
    def outlook_early(self, request, tag):
        return self.get_outlook(request, tag, "early")
