from twisted.web.template import Element, renderer, XMLFile, tags
from twisted.python.filepath import FilePath
from os.path import join
from datetime import datetime
import pathlib 

from mycorona.web.res.base import UberTabUS 

from . import TEMPLATEDIR

class USDeathsElement(UberTabUS, Element):
    
    TEMPLATE_FILE=join(TEMPLATEDIR,'deaths.xml')
    
    loader = XMLFile(FilePath(TEMPLATE_FILE))

    def __init__(self, *args, **kwargs):
        """Init tabname and href"""
        UberTabUS.__init__(self, "Death Toll", "ded.html")
        Element.__init__(self, *args, **kwargs)


    def get_outlook(self, request, tag, subsection, outlook):
        """
        Add all images in the specified dir to the html document
        """
        doc_rootpath=self.fs_root
        global_imgdir=doc_rootpath/subsection/"img"/outlook
        # add all images in directory, each wrapped in an href reference to the 
        # full picture
        for png in sorted(global_imgdir.glob("*deaths*.*")):
            img_src=str(png.relative_to(doc_rootpath))
            tag(tags.a(href=img_src)(tags.img(src=img_src, alt=png.name)))
        return tag

    
    @renderer
    def county_outlook(self, request, tag):
        return self.get_outlook(request, tag, "county","outlook")

    @renderer
    def county_developing(self, request, tag):
        return self.get_outlook(request, tag, "county","developing")
    
    @renderer
    def county_early(self, request, tag):
        return self.get_outlook(request, tag, "county","early")


    @renderer
    def state_outlook(self, request, tag):
        return self.get_outlook(request, tag, "state","outlook")

    @renderer
    def state_developing(self, request, tag):
        return self.get_outlook(request, tag, "state","developing")
    
    @renderer
    def state_early(self, request, tag):
        return self.get_outlook(request, tag, "state","early")
