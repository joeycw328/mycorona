from twisted.web.template import Element, renderer, XMLFile, tags
from datetime import datetime
import pathlib 
from os.path import join 
from collections import OrderedDict

from mycorona import web
from mycorona.web import ROOTPATH 

SOURCE="https://github.com/CSSEGISandData/COVID-19"
CODE="https://bitbucket.org/joeycw328/mycorona/src/master/"

HADOOP_URL="https://hadoop.apache.org/"
GIT_URL="https://git-scm.com/"

MATPLOTLIB_URL="https://matplotlib.org/"
ADJUSTTEXT_URL="https://github.com/Phlya/adjustText"
TWISTED_URL="https://twistedmatrix.com/trac/wiki/TwistedWeb"
RPI_URL="https://www.raspberrypi.org/products/raspberry-pi-4-model-b/"
class HeaderTab():

    __registry=[]

    def __init__(self, text, href):
        """
        Store which tab this is
        """
        self.href=href
        self.text=text
        self.pager=None
        self.target="Pandemic"
        self.register(self)
        self.__index=len(self.get_registry())-1

    @classmethod
    def get_registry(cls):
        return cls.__registry

    @classmethod 
    def register(cls, instance):
        cls.__registry.append(instance)

    @renderer
    def header(self, request, tag):
        """Render all tabs with links, except this tab"""
        pager=tags.h3 if not self.pager else self.pager
        pager("[")
        # add each tab as href, except this tab as string
        num_tabs=len(self.get_registry())
        for i,tab in enumerate(self.get_registry()):
            if tab==self: 
                pager(self.text)
            else: 
                pager(tags.a(tab.text, href=tab.href))
            if i<num_tabs-1:
                pager(" | ")
        pager(" ]")
        tag(pager(tags.h1('COVID-19 %s Livetracker - %s '%(self.target,self.text))))
        now_str=datetime.utcnow().strftime(web.DATE_FORMAT)
        src="Data: Novel Coronavirus (COVID-19) Cases, provided by JHU CSSE."
        tag(src)(tags.br)
        tag("Link: ")(tags.a(SOURCE, href=SOURCE))(tags.br)
        tag(tags.br)
        tag("Last Checked: %s"%now_str)(tags.br)
        tag(tags.br)
        tag("Author: Josef C. Willhite")(tags.br)
        tag("Source: ")(tags.a(CODE, href=CODE))(tags.br)
        return tag

    @renderer
    def footer(self, request, tag):
        tag(tags.br)(tags.br)
        tag("Computation performed using ")
        tag(tags.a("Apache Hadoop", href=HADOOP_URL))(" 3.2.1")(tags.br)
        tag("Figures rendered using ")(tags.a("Matplotlib", href=MATPLOTLIB_URL))
        tag(" with Phyla's ")(tags.a("adjustText", href=ADJUSTTEXT_URL))
        tag(", hosted using ")(tags.a("Twisted Web", href=TWISTED_URL))(tags.br)
        tag("Powered by ")(tags.a("Raspberry Pi 4", href=RPI_URL))
        return tags.em(tag)

class UberHeaderTab(HeaderTab):
    """Subclass to implement pager with subpager"""
    
    __registry=OrderedDict([])
    __registered_hrefs=set()
    def __init__(self, href_root, ubertext, text, leaf_doc):
        self.__uber=ubertext
        self.__root=pathlib.Path(href_root)
        self.__fsroot=pathlib.Path(join(ROOTPATH, self.doc_root.name))
        self.__leaf=leaf_doc
        href=self.__root/self.__leaf
        super(UberHeaderTab, self).__init__(text, str(href))
        self.target=self.__uber

    @property
    def doc_root(self):
        """Return a path object that points to root stub for html hrefs"""
        return self.__root

    @property 
    def fs_root(self):
        """
        Return a path object containing the full filesystem path to the directory 
        wherein resources for this tab will be stored 
        (e.g. /var/www/html/mycorona/global)
        """
        return self.__fsroot
    
    @property 
    def fspath(self):
        """Return a path object contining the full filesystem path to the html 
        document for this tab"""
        return self.__fsroot/self.__leaf
    
    
    @classmethod
    def register(cls, inst):
        if inst.__root not in cls.__registry:
            cls.__registry[inst.__root]={
                "uber":inst.__uber,
                "tabs":[]
            }
        cls.__registry[inst.__root]["tabs"].append(inst)
        cls.__registered_hrefs.add(inst.href)
    
    def get_registry(self):
        return self.__registry[self.__root]["tabs"]

    @renderer
    def header(self, request, tag):
        """Render all tabs with links, except this tab"""
        pager=tags.h3('[ ')
        # add each tab as href, except this tab as string
        roots=self.__registry.keys()
        num_roots=len(roots)
        for i,root in enumerate(roots):
            if root==self.__root: 
                pager(self.__uber)
            else: 
                ub_text=self.__registry[root]["uber"]
                ub_link=str(root/self.__leaf)
                if ub_link not in self.__registered_hrefs:
                    pager(ub_text)
                else:
                    pager(tags.a(ub_text, href=ub_link))
            if i<num_roots-1:
                pager(" | ")
        pager(" ] ")
        self.pager=pager
        return super(UberHeaderTab, self).header(request, tag)


class UberTabGlobal(UberHeaderTab):
    """Defaults to global document root"""
    def __init__(self, text, leaf_doc):
        root="/mmmmycorona/global/"
        uber="Global"
        super(UberTabGlobal, self).__init__(root, uber, text, leaf_doc)


class UberTabUS(UberHeaderTab):
    """Defaults to us document root"""
    def __init__(self, text, leaf_doc):
        root="/mmmmycorona/us/"
        uber="US"
        super(UberTabUS, self).__init__(root, uber, text, leaf_doc)
