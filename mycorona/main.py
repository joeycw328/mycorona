#!/usr/bin/env python3
import argparse
from argparse import ArgumentParser
from twisted.web.template import flattenString

# get global data pages
from mycorona.web.res.glbl.cases import CasesElement
from mycorona.web.res.glbl.deaths import DeathsElement

# get us data pages
from mycorona.web.res.us.cases import USCasesElement
from mycorona.web.res.us.deaths import USDeathsElement


from pathlib import Path
import os
from os.path import join,dirname

parser=ArgumentParser(
    description="MyCorona Figure renderer",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument("-d","--debug", action="store_true", help="render figures in current directory instead of production location")
parser.add_argument("-w","--workers", metavar="NUM", action="store", type=int, default=4, help="Number of render processes to use ")
parser.add_argument("-f","--force", action="store_true", help="Force figures to render, even if datasets contain no new data")
_args=None 

def make_renderer(element, name=""):
    """Return a function that can be used to render UberHeaderTab elements"""
    name=name if name else element.fspath
    def render_element(output):
        # decode the utf-8 binary into html
        element_html=output.decode("utf-8")
        # dump to html file
        element_path=element.fspath
        if not element_path.parent.exists():
            os.makedirs(str(element_path.parent))
        print("Updating %s: %s"%(name, element_path))
        element_path.write_text(element_html)
    return render_element


def check_superuser():
    path=Path("/var/www/html/.corona")
    try:
        path.touch()
        path.unlink()
    except PermissionError:
        print("Must have superuser permissions.")
        exit(2)
    except Exception as e:
        print("Error occcured during permission check: %s"%e)
        exit(1)
    except:
        raise


def getargs():
    global _args 
    if _args: return _args 
    _args=parser.parse_args()
    return _args

def update_date():
    """Update Johns Hopkins data using git"""

TABS=[
    CasesElement,
    DeathsElement,
    USCasesElement,
    USDeathsElement,
]

def update():
    """
    Update all figures from extant data and refresh web page html
    """
    args=getargs()
    # make sure we have permission to touch privileged directories
    if not args.debug: check_superuser()
    # render all figures
    from mycorona.figures import make_figures
    make_figures(args)
    if args.debug: return 
    # create all all web page objects, so the tab registry is populated
    elements=[]
    for tab in TABS: elements.append(tab())
    # render all elements
    for element in elements:
        renderer=make_renderer(element)
        flattenString(None, element).addCallback(renderer)
