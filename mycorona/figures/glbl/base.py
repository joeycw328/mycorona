"""
Base class for global figures
"""

from mycorona.web.figure.base import Figure

class GlobalFigure(Figure):
    """Mixin that provides unified interface(s) for all global figures"""
    installdir="global"