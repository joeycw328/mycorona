from os import path
import glob

# import all modules in current directory
modules = glob.glob(path.join(path.dirname(__file__), "*.py"))
__all__ = [ path.basename(f).split('.')[0] for f in modules if path.isfile(f) and not f.endswith('__init__.py')]
__all__ = [ module for module in __all__ if not module.startswith("_")]

from . import *

# clean local namespace
del glob 
del path 
del modules
