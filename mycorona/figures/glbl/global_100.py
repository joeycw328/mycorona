from mycorona.web import datasets
from mycorona.figures.glbl.base import GlobalFigure

class Global(GlobalFigure):
    """
    Base class for figures that use global data
    """
    xticks_     = (1,5)
    
    TOO_KURZ    = 21
    TOO_LONG    = -1

    def filter(self, record):
        """
        Filter records to return countries with >=20 days o
        """
        if self.is_logarithmic:  record.set_slice(record.slice.logarithmic)
        else: record.set_slice(record.slice.linear)
        country=record.country
        if "diamond" in country.lower(): return
        if self.TOO_KURZ and record.dlen-1 <= self.TOO_KURZ: return
        return record

    def xticks(self, longest_record):
        return self.xticks_


class GlobalNoChina(Global):
    """Subclass to get rid of China's data pollution"""
    def filter(self, record):
        """Override to remove China lulz"""
        country,y_data=record.country, record.data
        if "china" in country.lower(): return 
        return super(GlobalNoChina, self).filter(record)
        

class GlobalCases(Global):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    xlabel      = "Days (Since 100 Confirmed Cases)"
    ylabel      = "Total Confirmed Cases"
    title       = "COVID-19 Confirmed Cases [%s]"
    filename    = "global/cases_100_global%s.png"
    
class GlobalCasesNoChina(GlobalNoChina):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    xlabel      = "Days (Since 100 Confirmed Cases)"
    ylabel      = "Total Confirmed Cases"
    title       = "COVID-19 Confirmed Cases (-China) [%s]"
    filename    = "global/cases_100_global_nochina%s.png"


class GlobalCasesNoChinaHigh(GlobalCasesNoChina):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    title       = "COVID-19 Confirmed Cases (-China) [%s]\nHighest Confirmed Cases (σ > 0.5)"
    filename    = "global/cases_100_global_nochina_high%s.png"
    
    def filter(self, record):
        """Override to remove China lulz"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev and stddev<0.5: return 
        return super(GlobalCasesNoChinaHigh, self).filter(record)
        
class GlobalCasesNoChinaAbove(GlobalCasesNoChina):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    title       = "COVID-19 Confirmed Cases (-China) [%s]\nAbove Average Confirmed Cases (0 < σ < 0.5)"
    filename    = "global/cases_100_global_nochina_above%s.png"
    
    def filter(self, record):
        """Override to remove China lulz"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev>=0.5 or stddev<=0.: return 
        return super(GlobalCasesNoChinaAbove, self).filter(record)
        
class GlobalCasesNoChinaBelow(GlobalCasesNoChina):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    title       = "COVID-19 Confirmed Cases (-China) [%s]\nBelow Average Confirmed Cases (σ < 0)"
    filename    = "global/cases_100_global_nochina_below%s.png"
    
    def filter(self, record):
        """Override to remove China lulz"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev>0.0: return 
        return super(GlobalCasesNoChinaBelow, self).filter(record)
        

class GlobalDeaths(Global):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    title       = "COVID-19 Death Toll [%s]"
    filename    = "global/deaths_100_global%s.png"
    

class GlobalDeathsNoChina(GlobalNoChina):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    title       = "COVID-19 Death Toll (-China) [%s]"
    filename    = "global/deaths_100_global_nochina%s.png"
    


# instantiate case figures without china
cases=datasets.glbl.cases.sliced
global_cases_nochina_high_linear=GlobalCasesNoChinaHigh(cases).arranged()
global_cases_nochina_high_logarithmic=GlobalCasesNoChinaHigh(cases, logarithmic=True).arranged()
global_cases_nochina_above_linear=GlobalCasesNoChinaAbove(cases).arranged()
global_cases_nochina_above_logarithmic=GlobalCasesNoChinaAbove(cases, logarithmic=True).arranged()
global_cases_nochina_below_linear=GlobalCasesNoChinaBelow(cases).arranged()
global_cases_nochina_below_logarithmic=GlobalCasesNoChinaBelow(cases, logarithmic=True).arranged()

# instantiate case figures with china included
# global_cases_linear=GlobalCases(cases)
# global_cases_logarithmic=GlobalCases(cases, logarithmic=True)

# instantiate deaths figure(s) w/o china
deaths=datasets.glbl.deaths.sliced
global_deaths_nochina_linear=GlobalDeathsNoChina(deaths).arranged()
global_deaths_nochina_logarithmic=GlobalDeathsNoChina(deaths, logarithmic=True).arranged().without_danger_max()

# instantiate death figures with china included
# global_deaths_linear=GlobalDeaths(deaths)
# global_deaths_logarithmic=GlobalDeaths(deaths, logarithmic=True)
