from mycorona.web import datasets
from mycorona.figures.glbl.base import GlobalFigure

import math 

class GlobalDeveloping(GlobalFigure):
    """
    Figure containg early developments of global data, using linear scale
    """
    xlabel      = "Days (Since 100 Confirmed Cases)"
    ylabel      = "Total Confirmed Cases"
    title       = "COVID-19 Confirmed Cases\nDeveloping Outbreaks [%s]"
    filename    = "developing/cases_100_global_developing%s.png"
    
    # minimum length for this figure is 21 days (22 with 0-indexing)
    minlen = 22

    def filter(self, record):
        """
        Filter records to return countries with 7-20 days of outbreak
        """
        # country,y_data=record.country, record.data
        country=record.country
        if "diamond" in country.lower(): return
        # records with more than 3 weeks of data go in a different figure (0-indexed)
        if record.dlen-1>21: return
        # check high score (note: logarithmic slice data is used)
        record.set_slice(record.slice.linear)
        high_score=record.maxy
        # swap back to log data if this is a logarithmic figure
        if self.is_logarithmic: record.set_slice(record.slice.logarithmic)
        # record with less than a week of data go in a different figure (unless 
        # they're smashing, then they make this chart)
        if record.dlen-1<=7 and high_score<1000: return
        return record


class GlobalDevelopingCasesHigh(GlobalDeveloping):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"
    xlabel      = "Days (Since 100 Confirmed Cases)"
    title       = "COVID-19 Confirmed Cases (<= 21 Days Elapsed)\nHigh Growth (σ > 0.5) [%s]"
    filename    = "developing/cases_100_global_developing_high%s.png"
    def filter(self, record):
        """Override to remove China lulz"""
        stddev=record.stddev()
        if stddev and stddev<0.5: return 
        return super(GlobalDevelopingCasesHigh, self).filter(record)
        

class GlobalDevelopingCasesAbove(GlobalDeveloping):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"
    xlabel      = "Days (Since 100 Confirmed Cases)"
    title       = "COVID-19 Confirmed Cases (<= 21 Days Elapsed)\nAbove Average Growth (0 < σ < 0.5) [%s]"
    filename    = "developing/cases_100_global_developing_above%s.png"
    def filter(self, record):
        """Override to remove China lulz"""
        stddev=record.stddev()
        if not stddev: return
        if stddev>=0.5 or stddev<=0.0: return 
        return super(GlobalDevelopingCasesAbove, self).filter(record)
        

class GlobalDevelopingCasesBelow(GlobalDeveloping):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"
    xlabel      = "Days (Since 100 Confirmed Cases)"
    title       = "COVID-19 Confirmed Cases (<= 21 Days Elapsed)\nBelow Average Growth (σ < 0)  [%s]"
    filename    = "developing/cases_100_global_developing_below%s.png"
    def filter(self, record):
        """Override to remove China lulz"""
        stddev=record.stddev()
        if not stddev: return
        if stddev>0.0: return 
        return super(GlobalDevelopingCasesBelow, self).filter(record)
        
class GlobalRisingDeaths(GlobalDeveloping):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    title       = "COVID-19 Global Death Toll\n<= 21 Days Elapsed [%s]"
    filename    = "developing/deaths_100_global_rising%s.png"



# instantiate case figure(s)
cases=datasets.glbl.cases.sliced

global_developing_cases_high_linear=GlobalDevelopingCasesHigh(cases).with_danger_max().arranged()
global_developing_cases_high_logarithmic=GlobalDevelopingCasesHigh(cases, logarithmic=True).without_danger_max().arranged()
global_developing_cases_above_linear=GlobalDevelopingCasesAbove(cases).with_danger_max().arranged()
global_developing_cases_above_logarithmic=GlobalDevelopingCasesAbove(cases, logarithmic=True).without_danger_max().arranged()
global_developing_cases_below_linear=GlobalDevelopingCasesBelow(cases).with_danger_max().arranged()
global_developing_cases_below_logarithmic=GlobalDevelopingCasesBelow(cases, logarithmic=True).without_danger_max().arranged()




# instantiate death figure(s)
deaths=datasets.glbl.deaths.sliced
global_rising_deaths_linear=GlobalRisingDeaths(deaths).with_danger_max().arranged()
global_rising_deaths_linear=GlobalRisingDeaths(deaths, logarithmic=True).without_danger_max().arranged()