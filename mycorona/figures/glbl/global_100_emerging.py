import math

from mycorona.web import datasets
from mycorona.web.figure.util import danger_line
from mycorona.figures.glbl.base import GlobalFigure


# how long to consider an outbreak emerging
EMERGING_DURATION=7 

# store the high risk barrier at 14.8698354% growth. At this rate, deaths double 
# every 4 days
_HIGH=(0.148698354)*100.0

class GlobalEmerging(GlobalFigure):
    """
    Figure containg emerging developments of global data, using linear scale
    """

    # minimum x length for this figure is 7 days (which is 8 with 0 indexed data)
    minlen=8
    
    def __init__(self, datastore, logarithmic=False, high=True):
        """
        Global outlook figure with option to produce figure in logarithmic scale
        """
        self.__high=high
        self.title=self.title%("High" if high else "Low")
        self.title+="  [%s]"
        self.filename=self.filename%("hi" if high else "lo")
        self.filename+="%s.png"
        super(GlobalEmerging, self).__init__(datastore, logarithmic)

    
    def filter(self, record):
        """
        Filter records to return emerging developments only
        """
        country=record.country
        # fuck cruise ships polluting my data!
        if "diamond" in country.lower(): return
        # this figure is only for emerging outbreaks!
        if record.dlen-1>EMERGING_DURATION: return
        # high growth and low growth go in different figures
        if not record.score: return 
        if self.__high and record.score < _HIGH: return 
        if not self.__high and record.score >=_HIGH: return
        # note that the aforementioned segretation is based on the logarithmic 
        # slice. If we're in a linear figure, swap to the linear slice
        if not self.is_logarithmic: 
            record.set_slice(record.slice.linear)
        return record



class GlobalEmergingCases(GlobalEmerging):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"
    xlabel      = "Days (Since 100 Confirmed Cases)"
    title       = "COVID-19 Confirmed Cases (<= 7 Days Elapsed)\n%s Daily Growth"
    filename    = "early/cases_100_global_emerging_%s"


class GlobalEarlyDeaths(GlobalEmerging):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    title       = "COVID-19 Global Death Toll (<= 7 Days Elapsed)\n%s Daily Growth"
    filename    = "early/deaths_100_global_emerging_%s"



# instantiate confirmed case figure(s)
cases=datasets.glbl.cases.sliced
global_emerging_cases_linear_hi=GlobalEmergingCases(cases).arranged()
global_emerging_cases_logarithmic_hi=GlobalEmergingCases(cases, logarithmic=True).without_danger_max().arranged()
global_emerging_cases_linear_low=GlobalEmergingCases(cases, high=False).without_danger_max().arranged()
global_emerging_cases_logarithmic_low=GlobalEmergingCases(cases, logarithmic=True, high=False).without_danger_max().arranged()

# instantiate confirmed death figure(s)
deaths=datasets.glbl.deaths.sliced
global_early_deaths_linear_hi=GlobalEarlyDeaths(deaths).without_danger_max().arranged()
global_early_deaths_logarithmic_hi=GlobalEarlyDeaths(deaths, logarithmic=True).without_danger_max().arranged()
global_early_deaths_linear_low=GlobalEarlyDeaths(deaths, high=False).without_danger_max().arranged()
global_early_deaths_logarithmic_low=GlobalEarlyDeaths(deaths, logarithmic=True, high=False).without_danger_max().arranged()
