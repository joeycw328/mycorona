from mycorona.web import datasets
from mycorona.figures.us.us import USCountyFigure

class County(USCountyFigure):
    """
    Base class for figures that use global data
    """
    xlabel      = "Days (Since 100 Confirmed Cases)"
    ylabel      = "Total Confirmed Cases"
    
    xticks_     = (1,5)
    
    TOO_KURZ    = 21
    TOO_LONG    = -1

    def filter(self, record):
        """ Filter records to return counties with >21 days of data """
        if self.is_logarithmic:  record.set_slice(record.slice.logarithmic)
        else: record.set_slice(record.slice.linear)
        # if self.TOO_KURZ and record.dlen-1 <= self.TOO_KURZ: return
        return super(County, self).filter(record)

    def xticks(self, longest_record):
        return self.xticks_


class CountyCasesHighest(County):
    """
    Class for providing count case count, in logarithmic and linear scales
    """
    title       = "COVID-19 Confirmed Cases - US Counties [%s]\nHighest Confirmed Cases (σ > 1.0)"
    filename    = "outlook/cases_100_highest%s.png"
    
    def filter(self, record):
        """Use stddev filter to get highest count by stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev and stddev<1: return 
        return super(CountyCasesHighest, self).filter(record)
      
class CountyCasesHigh(County):
    """
    Class for providing count case count, in logarithmic and linear scales
    """
    title       = "COVID-19 Confirmed Cases - US Counties [%s]\nHigh Confirmed Cases (0.5 < σ < 1)"
    filename    = "outlook/cases_100_high%s.png"
    
    def filter(self, record):
        """Use stddev filter to get high count by stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev>=1.0 or stddev<0.5: return 
        return super(CountyCasesHigh, self).filter(record)
      
class CountyCasesAbove(County):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    title       = "COVID-19 Confirmed Cases - US Counties [%s]\nAbove Average Confirmed Cases (0.25 < σ < 0.5)"
    filename    = "outlook/cases_100_below%s.png"
    
    def filter(self, record):
        """Use stddev filter to get below average count by stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev>0.5 or stddev<0.25: return 
        return super(CountyCasesAbove, self).filter(record)

class CountyDead:
    """
    Base class for figures that use county death data
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    

class CountyDeathsHighest(CountyDead, CountyCasesHighest):
    title       = "COVID-19 Death Toll - US Counties [%s]\nHighest Death Toll (σ > 1.0)"
    filename    = "outlook/deaths_100_highest%s.png"

class CountyDeathsHigh(CountyDead, CountyCasesHigh):
    title       = "COVID-19 Death Toll - US Counties [%s]\nHigh Death Toll (0.5 < σ < 1)"
    filename    = "outlook/deaths_100_high%s.png"
  
  
class CountyDeathsAbove(CountyDead, CountyCasesAbove):
    title       = "COVID-19 Death Toll - US Counties [%s]\nAbove Average Death Toll (0.25 < σ < 0.5)"
    filename    = "outlook/deaths_100_above%s.png"


# instantiate county case figures 
cases=datasets.us.county.cases.sliced
us_county_cases_highest_linear=CountyCasesHighest(cases).arranged()
us_county_cases_highest_logarithmic=CountyCasesHighest(cases, logarithmic=True).arranged()
us_county_cases_high_linear=CountyCasesHigh(cases).arranged()
us_county_cases_high_logarithmic=CountyCasesHigh(cases, logarithmic=True).arranged()
us_county_cases_above_linear=CountyCasesAbove(cases).arranged()
us_county_cases_above_logarithmic=CountyCasesAbove(cases, logarithmic=True).arranged()

# instantiate county death figures 
deaths=datasets.us.county.deaths.sliced
us_county_deaths_highest_linear=CountyDeathsHighest(deaths).arranged()
us_county_deaths_highest_logarithmic=CountyDeathsHighest(deaths, logarithmic=True).arranged().without_danger_max()
us_county_deaths_high_linear=CountyDeathsHigh(cases).arranged()
us_county_deaths_high_logarithmic=CountyDeathsHigh(cases, logarithmic=True).arranged()
us_county_deaths_above_linear=CountyDeathsAbove(cases).arranged()
us_county_deaths_above_logarithmic=CountyDeathsAbove(cases, logarithmic=True).arranged()
