"""
Base class for global figures
"""

from mycorona.web.figure.base import Figure
from .states2 import STATES2 

class USCountyPopulationFigure(Figure):
    """Mixin that provides unified interface(s) for US county figures (population biased data)"""
    installdir="us/county"
    
    # stash 2-letter states for each states
    STATES2=STATES2
    DANGER_LINES=None
    def filter(self, record):
        """Catchall filter for all us county data"""
        # update record label to county, state 
        state=record.state
        if state in self.STATES2: state=self.STATES2[state]
        record.label="%s, %s"%(record.county, state)
        # check for logarithmicness just in case
        if self.is_logarithmic: record.set_slice(record.slice.logarithmic)
        else: record.set_slice(record.slice.linear)
        return record


class USStatePopulationFigure(Figure):
    """Mixin that provides unified interface(s) for US state figures (population biased data)"""
    installdir="us/state"
    DANGER_LINES=None
    def filter(self, record):
        """Catchall filter for all us state data"""
        if self.is_logarithmic: record.set_slice(record.slice.logarithmic)
        else: record.set_slice(record.slice.linear)
        return record
