from mycorona.web import datasets
from mycorona.figures.us.us import USCountyFigure

# how long to consider an outbreak emerging
EMERGING_DURATION=7 

# store the high risk barrier at 14.8698354% growth. At this rate, deaths double 
# every 4 days
_HIGH=(0.148698354)*100.0

class CountyEmerging(USCountyFigure):
    """
    Figure containg emerging developments of county data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"   
    xlabel      = "Days (Since 100 Confirmed Cases)"
    
    # minimum x length for this figure is 7 days (which is 8 with 0 indexed data)
    minlen=8
    
    # def __init__(self, datastore, logarithmic=False, high=True):
    #     """
    #     County outlook figure with option to produce figure in logarithmic scale
    #     """
    #     self.__high=high
    #     self.title=self.title%("High" if high else "Low")
    #     self.title+="  [%s]"
    #     self.filename=self.filename%("hi" if high else "lo")
    #     self.filename+="%s.png"
    #     super(CountyEmerging, self).__init__(datastore, logarithmic)

    
    def filter(self, record):
        """
        Filter records to return emerging developments only
        """
        county=record.county
        # fuck cruise ships polluting my data!
        if "diamond" in county.lower(): return
        # this figure is only for emerging outbreaks!
        if record.dlen-1>EMERGING_DURATION: return
        # high growth and low growth go in different figures
        # if not record.score: return 
        # # if self.__high and record.score < _HIGH: return 
        # if self.__high and record.stddev() < 0.5: return 
        # if not self.__high and (record.stddev() > 0.5 or record.stddev()<0.0): return
        # note that the aforementioned segretation is based on the logarithmic 
        # slice. If we're in a linear figure, swap to the linear slice
        # if not self.is_logarithmic: 
        #     record.set_slice(record.slice.linear)
        return super(CountyEmerging, self).filter(record)



class CountyEmergingCasesHigh(CountyEmerging):
    """
    Figure containg early developments of global data, using linear scale
    """
    title       = "COVID-19 Confirmed Cases (<= 7 Days Elapsed)\nHigh Growth Rate (σ > 0.5) [%s]"
    filename    = "early/cases_100_emerging_high%s.png"
    def filter(self, record):
        """ Filter records to return emerging developments only """
        stddev=record.stddev()
        if not stddev: return 
        if stddev < 0.5: return 
        return super(CountyEmergingCasesHigh, self).filter(record)

class CountyEmergingCasesAbove(CountyEmerging):
    """
    Figure containg early developments of global data, using linear scale
    """
    title       = "COVID-19 Confirmed Cases (<= 7 Days Elapsed)\nAbove Average Growth Rate (0 < σ < 0.5) [%s]"
    filename    = "early/cases_100_emerging_above%s.png"
    def filter(self, record):
        """ Filter records to return emerging developments only """
        stddev=record.stddev()
        if not stddev: return 
        if stddev > 0.5 or stddev<0.0: return 
        return super(CountyEmergingCasesAbove, self).filter(record)


class CountyEmergingDead:
    """
    Base class for figures that use county death data
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    
# class CountyEmergingDeathsHighest(CountyEmergingDead, CountyEmergingCasesHighest):
#     title       = "COVID-19 Death Toll (<= 7 Days Elapsed)\nHighest Growth Rate (σ > 1.0)"
#     filename    = "outlook/deaths_100_highest%s.png"

class CountyEmergingDeathsHigh(CountyEmergingDead, CountyEmergingCasesHigh):
    title       = "COVID-19 Death Toll (<= 7 Days Elapsed)\nHigh Growth Rate (σ > 0.5) [%s]"
    filename    = "early/deaths_100_emerging_high%s.png"
  
  
class CountyEmergingDeathsAbove(CountyEmergingDead, CountyEmergingCasesAbove):
    title       = "COVID-19 Death Toll (<= 7 Days Elapsed)\nAbove Average Growth Rate (0 < σ < 0.5) [%s]"
    filename    = "early/deaths_100_emerging_above%s.png"


# instantiate confirmed case figure(s)
cases=datasets.us.county.cases.sliced
global_emerging_cases_linear_hi=CountyEmergingCasesHigh(cases).arranged()
global_emerging_cases_logarithmic_hi=CountyEmergingCasesHigh(cases, logarithmic=True).without_danger_max().arranged()
global_emerging_cases_linear_low=CountyEmergingCasesAbove(cases).without_danger_max().arranged()
global_emerging_cases_logarithmic_low=CountyEmergingCasesAbove(cases, logarithmic=True).without_danger_max().arranged()

# instantiate death figure(s)
deaths=datasets.us.county.deaths.sliced
us_county_emerging_deaths_high_linear=CountyEmergingDeathsHigh(deaths).with_danger_max().arranged()
us_county_emerging_deaths_high_logarithmic=CountyEmergingDeathsHigh(deaths, logarithmic=True).arranged()
us_county_emerging_deaths_above_linear=CountyEmergingDeathsAbove(deaths).with_danger_max().arranged()
us_county_emerging_deaths_above_logarithmic=CountyEmergingDeathsAbove(deaths, logarithmic=True).arranged()
