from mycorona.web import datasets
from mycorona.figures.us.us import USCountyFigure

import math 

class CountyDeveloping(USCountyFigure):
    """
    Figure containg early developments of global data, using linear scale
    """
    xlabel      = "Days (Since 100 Confirmed Cases)"
    ylabel      = "Total Confirmed Cases"
    title       = "COVID-19 Confirmed Cases\nDeveloping Outbreaks [%s]"
    # filename    = "developing/cases_100_developing%s.png"
    
    # minimum length for this figure is 21 days (22 with 0-indexing)
    # minlen = 22

    def filter(self, record):
        """
        Filter records to return countries with 7-20 days of outbreak
        """
        county=record.county
        # records with more than 3 weeks of data go in a different figure (0-indexed)
        if record.dlen-1>21: return
        # check high score (note: logarithmic slice data is used)
        record.set_slice(record.slice.linear)
        high_score=record.maxy
        # figures with linear stddev >0.25 go in top figures 
        stddev=record.stddev() 
        if stddev > 0.25: return
        # swap back to log data if this is a logarithmic figure
        # record with less than a week of data go in a different figure (unless 
        # they're smashing, then they make this chart)
        if record.dlen-1<=7 and high_score<1000: return
        return super(CountyDeveloping, self).filter(record)


class CountyDevelopingCasesHighest(CountyDeveloping):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"
    xlabel      = "Days (Since 100 Confirmed Cases)"
    title       = "COVID-19 Low Confirmed Cases (σ < 0.25)\nHighest Growth Rate (σ > 0.5) [%s]"
    filename    = "developing/cases_100_developing_highest%s.png"
    def filter(self, record):
        """Override to remove China lulz"""
        # record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev and stddev<0.5: return 
        return super(CountyDevelopingCasesHighest, self).filter(record)
        

class CountyDevelopingCasesHigh(CountyDeveloping):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"
    xlabel      = "Days (Since 100 Confirmed Cases)"
    title       = "COVID-19 Low Confirmed Cases (σ < 0.25)\nHigh Growth Rate (0.25 < σ < 0.5) [%s]"
    filename    = "developing/cases_100_developing_high%s.png"
    def filter(self, record):
        """Override to remove China lulz"""
        # record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev>=0.5 or stddev<=0.25: return 
        return super(CountyDevelopingCasesHigh, self).filter(record)
        

class CountyDevelopingCasesAbove(CountyDeveloping):
    """
    Figure containg early developments of global data, using linear scale
    """
    ylabel      = "Total Confirmed Cases"
    xlabel      = "Days (Since 100 Confirmed Cases)"
    title       = "COVID-19 Low Confirmed Cases (σ < 0.25)\nAbove Average Growth Rate (0 < σ < 0.25)  [%s]"
    filename    = "developing/cases_100_developing_above%s.png"
    def filter(self, record):
        """Override to remove China lulz"""
        # record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev>=0.25 or stddev<=0.0: return 
        return super(CountyDevelopingCasesAbove, self).filter(record)


class CountyDevelopingDead(USCountyFigure):
    """
    Base class for figures that use county death data
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    def filter(self, record):
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev>=0.25: return 
        return super(CountyDevelopingDead, self).filter(record)

class CountyDevelopingDeathsHigh(CountyDevelopingDead):
    title       = "COVID-19 Death Toll (σ < 0.25) - US Counties\nHigh Growth Rate (σ > 0.5) [%s]"
    filename    = "developing/deaths_100_developing_high%s.png"
    def filter(self, record):
        # record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev < 0.5: return 
        return super(CountyDevelopingDeathsHigh, self).filter(record)

class CountyDevelopingDeathsAbove(CountyDevelopingDead):
    title       = "COVID-19 Death Toll (σ < 0.25) - US Counties\nAbove Average Growth Rate (0 < σ < 0.5) [%s]"
    filename    = "developing/deaths_100_developing_above%s.png"
    def filter(self, record):
        # record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev < 0.0 or stddev > 0.5: return 
        return super(CountyDevelopingDeathsAbove, self).filter(record)

class CountyDevelopingDeathsBelow(CountyDevelopingDead):
    title       = "COVID-19 Death Toll (σ < 0.25) - US Counties\nBelow Average Growth Rate (σ < 0) [%s]"
    filename    = "developing/deaths_100_developing_below%s.png"
    def filter(self, record):
        # record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if not stddev: return
        if stddev>0.0: return 
        return super(CountyDevelopingDeathsBelow, self).filter(record)




# instantiate case figure(s)
cases=datasets.us.county.cases.sliced
us_county_developing_cases_highest_linear=CountyDevelopingCasesHighest(cases).with_danger_max().arranged()
us_county_developing_cases_highest_logarithmic=CountyDevelopingCasesHighest(cases, logarithmic=True).arranged()
us_county_developing_cases_high_linear=CountyDevelopingCasesHigh(cases).with_danger_max().arranged()
us_county_developing_cases_high_logarithmic=CountyDevelopingCasesHigh(cases, logarithmic=True).arranged()
us_county_developing_cases_above_linear=CountyDevelopingCasesAbove(cases).with_danger_max().arranged()
us_county_developing_cases_above_logarithmic=CountyDevelopingCasesAbove(cases, logarithmic=True).arranged()


# instantiate death figure(s)
deaths=datasets.us.county.deaths.sliced
us_county_developing_deaths_high_linear=CountyDevelopingDeathsHigh(deaths).with_danger_max().arranged()
us_county_developing_deaths_high_logarithmic=CountyDevelopingDeathsHigh(deaths, logarithmic=True).without_danger_max().arranged()
us_county_developing_deaths_above_linear=CountyDevelopingDeathsAbove(deaths).with_danger_max().arranged()
us_county_developing_deaths_above_logarithmic=CountyDevelopingDeathsAbove(deaths, logarithmic=True).arranged()
us_county_developing_deaths_below_linear=CountyDevelopingDeathsBelow(deaths).with_danger_max().arranged()
us_county_developing_deaths_below_logarithmic=CountyDevelopingDeathsBelow(deaths, logarithmic=True).arranged()
