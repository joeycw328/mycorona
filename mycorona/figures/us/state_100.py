from mycorona.web import datasets
from mycorona.figures.us.us import USStateFigure

class State(USStateFigure):
    """Base class for figures that use state data """
    xlabel      = "Days (Since 100 Confirmed Cases)"
    ylabel      = "Total Confirmed Cases"
    
    xticks_     = (1,5)
    
    TOO_KURZ    = 21
    TOO_LONG    = -1

    def filter(self, record):
        # filter out short records
        # if record.dlen-1 <= self.TOO_KURZ: return 
        return super(State, self).filter(record)
    
    def xticks(self, longest_record):
        return self.xticks_


class StateCasesHigh(State):
    title       = "COVID-19 Confirmed Cases - US States [%s]\nHigh Confirmed Cases (σ > 0.5)"
    filename    = "outlook/cases_100_high%s.png"
    
    def filter(self, record):
        """Use stddev filter to get highest count by stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev and stddev<0.5: return 
        return super(StateCasesHigh, self).filter(record)
      
class StateCasesAbove(State):
    title       = "COVID-19 Confirmed Cases - US States [%s]\nAbove Average Confirmed Cases (0 < σ < 0.5)"
    filename    = "outlook/cases_100_above%s.png"
    
    def filter(self, record):
        """Use stddev filter to get above avg by stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev>0.5 or stddev<0: return 
        return super(StateCasesAbove, self).filter(record)
      



class StateDeathsHigh(State):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    title       = "COVID-19 Death Toll - US States [%s]\nHigh Death Toll (σ > 0.5)"
    filename    = "outlook/deaths_100_high%s.png"
    def filter(self, record):
        """Use stddev filter to get above avg by stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev<0.5: return 
        return super(StateDeathsHigh, self).filter(record)
      

class StateDeaths(State):
    """
    Class for providing global case count, in logarithmic and linear scales
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    title       = "COVID-19 Death Toll - US States [%s]\nLow Death Toll (σ < 0.5)"
    filename    = "outlook/deaths_100_low%s.png"
    def filter(self, record):
        """Use stddev filter to get above avg by stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev>0.5: return 
        return super(StateDeaths, self).filter(record)
      

# instantiate State case figures 
cases=datasets.us.state.cases.sliced
us_state_cases_high_linear=StateCasesHigh(cases).arranged()
us_state_cases_high_logarithmic=StateCasesHigh(cases, logarithmic=True).arranged()
us_state_cases_above_linear=StateCasesAbove(cases).arranged()
us_state_cases_above_logarithmic=StateCasesAbove(cases, logarithmic=True).arranged()

# instantiate State death figures 
deaths=datasets.us.state.deaths.sliced
us_state_deaths_high_linear=StateDeathsHigh(deaths).arranged()
us_state_deaths_high_logarithmic=StateDeathsHigh(deaths, logarithmic=True).arranged().without_danger_max()
us_state_deaths_low_linear=StateDeaths(deaths).arranged()
us_state_deaths_low_logarithmic=StateDeaths(deaths, logarithmic=True).arranged().without_danger_max()
