from mycorona.web import datasets
from mycorona.figures.us.us import USStateFigure

class StateDeveloping(USStateFigure):
    """
    Figure containg early developments of global data, using linear scale
    """
    xlabel      = "Days (Since 100 Confirmed Cases)"
    ylabel      = "Total Confirmed Cases"
    def filter(self, record):
        """
        Filter records to return countries with 7-20 days of outbreak
        """
        # county=record.county
        # records with more than 3 weeks of data go in a different figure (0-indexed)
        # if record.dlen-1>21: return
        # # check high score (note: logarithmic slice data is used)
        record.set_slice(record.slice.linear)
        # high_score=record.maxy
        # figures with linear stddev >0.0 go in top state figures 
        stddev=record.stddev() 
        if stddev > 0.0: return
        # swap back to log data if this is a logarithmic figure
        # record with less than a week of data go in a different figure (unless 
        # they're smashing, then they make this chart)
        # if record.dlen-1<=7 and high_score<1000: return
        return super(StateDeveloping, self).filter(record)


# class StateDevelopingCasesHighest(StateDeveloping):
#     """
#     Figure containg early developments of state data, using linear scale
#     """
#     ylabel      = "Total Confirmed Cases"
#     xlabel      = "Days (Since 100 Confirmed Cases)"
#     title       = "COVID-19 Low Confirmed Cases (σ < 0.25)\nHighest Growth Rate (σ > 0.5) [%s]"
#     filename    = "developing/cases_100_developing_highest%s.png"
#     def filter(self, record):
#         """Override to remove China lulz"""
#         # record.set_slice(record.slice.linear)
#         stddev=record.stddev()
#         if stddev and stddev<0.5: return 
#         return super(StateDevelopingCasesHighest, self).filter(record)
        



class StateDevelopingCasesAbove(StateDeveloping):
    """States with low case count, but high growth"""
    title       = "COVID-19 Low Confirmed Cases (σ < 0)\nAbove Average Growth Rate (σ > 0)  [%s]"
    filename    = "developing/cases_100_developing_above%s.png"
    def filter(self, record):
        stddev=record.stddev()
        if not stddev: return
        if stddev<0.0: return 
        return super(StateDevelopingCasesAbove, self).filter(record)
        
class StateDevelopingCasesBelow(StateDeveloping):
    """States with low case count, and low growth"""
    title       = "COVID-19 Low Confirmed Cases (σ < 0)\nBelow Average Growth Rate (σ < 0) [%s]"
    filename    = "developing/cases_100_developing_below%s.png"
    def filter(self, record):
        stddev=record.stddev()
        if not stddev: return
        if stddev>0.0: return 
        return super(StateDevelopingCasesBelow, self).filter(record)

class StateDevelopingDead(USStateFigure):
    """
    Base class for figures that use state death data
    """
    ylabel      = "Total Dead"
    xlabel      = "Days (Since 100 Dead)"
    def filter(self, record):
        """Get rid of state data with > 0.5 stddev"""
        record.set_slice(record.slice.linear)
        stddev=record.stddev()
        if stddev>0.5: return 
        return super(StateDevelopingDead, self).filter(record)



class StateDevelopingDeathsHigh(StateDevelopingDead):
    title       = "COVID-19 Low Death Toll (σ < 0.5)\nHigh Growth Rate (σ > 0.5) [%s]"
    filename    = "developing/deaths_100_developing_high%s.png"
    def filter(self, record):
        """Filter by high growth rate"""
        stddev=record.stddev()
        if not stddev: return 
        if stddev<0.5: return 
        return super(StateDevelopingDeathsHigh, self).filter(record)
      
  
class StateDevelopingDeathsAbove(StateDevelopingDead):
    title       = "COVID-19 Low Death Toll (σ < 0.5)\nAbove Average Growth Rate (0 < σ < 0.5) [%s]"
    filename    = "developing/deaths_100_developing_above%s.png"
    def filter(self, record):
        """Filter by high growth rate"""
        stddev=record.stddev()
        if not stddev: return 
        if stddev < 0.0 or stddev>0.5: return 
        return super(StateDevelopingDeathsAbove, self).filter(record)
      

# instantiate case figure(s)
cases=datasets.us.state.cases.sliced
us_state_developing_cases_above_linear=StateDevelopingCasesAbove(cases).with_danger_max().arranged()
us_state_developing_cases_above_logarithmic=StateDevelopingCasesAbove(cases, logarithmic=True).arranged()
us_state_developing_cases_below_linear=StateDevelopingCasesBelow(cases).with_danger_max().arranged()
us_state_developing_cases_below_logarithmic=StateDevelopingCasesBelow(cases, logarithmic=True).arranged()



# instantiate death figure(s)
deaths=datasets.us.state.deaths.sliced
us_state_developing_deaths_high_linear=StateDevelopingDeathsHigh(deaths).with_danger_max().arranged()
us_state_developing_deaths_high_logarithmic=StateDevelopingDeathsHigh(deaths, logarithmic=True).without_danger_max().arranged()
us_state_developing_deaths_above_linear=StateDevelopingDeathsAbove(deaths).with_danger_max().arranged()
us_state_developing_deaths_above_logarithmic=StateDevelopingDeathsAbove(deaths, logarithmic=True).without_danger_max().arranged()
