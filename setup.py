from setuptools import setup, find_packages
from os.path import join, basename, exists

from mycorona.web import CONFDIR as MYC_CONFDIR


HADOOP_ROOT="//home/atomsk/.local/"
HADOOP_CONF_DIR=join(HADOOP_ROOT,"etc","hadoop")

ENTRY_POINTS={
    "console_scripts": [
        "mycorona-update = mycorona.main:update",
        "mycorona-mapcsv = mycorona.livetrack.entry:enter_mapcsv",
        "mycorona-reducer = mycorona.livetrack.entry:enter_reducer",
    ]
}


HADOOP_FILES=[
    'linux/hadoop/capacity-scheduler.xml',
    'linux/hadoop/core-site.xml',
    'linux/hadoop/hdfs-site.xml',
    'linux/hadoop/httpfs-site.xml',
    'linux/hadoop/kms-site.xml',
    'linux/hadoop/mapred-site.xml',
    'linux/hadoop/yarn-site.xml',
    'linux/hadoop/workers',
] 
# make sure not to overwrite existing configuration files
HADOOP_DIST=[]
for filename in HADOOP_FILES:
    conf_file=join(HADOOP_CONF_DIR, basename(filename))
    # if not exists(conf_file): 
    HADOOP_DIST.append(filename)

DATA_FILES=[
    ('//usr/lib/systemd/system', 
        [
            'linux/systemd/mycorona-data-fetch.service', 
            'linux/systemd/mycorona-data-import.service', 
            'linux/systemd/mycorona-data-compile.service', 
            'linux/systemd/x-data-compile@.service', 
            'linux/systemd/mycorona-data-export.service', 
            'linux/systemd/mycorona-warehouse-cleanup@.service', 
            'linux/systemd/mycorona-etl.target', 
            'linux/systemd/mycorona-etl.service', 
            'linux/systemd/mycorona-web-update.service', 
            'linux/systemd/livetrack-update.target', 
            'linux/systemd/livetrack-update.service', 
            'linux/systemd/livetrack-update.timer', 
            'linux/systemd/livetrack.service', 
            'linux/systemd/warehouse/dfs/dfs-bootstrap@.service', 
            'linux/systemd/warehouse/dfs/dfs-datanode@.service', 
            'linux/systemd/warehouse/dfs/dfs-namenode-primary@.service', 
            'linux/systemd/warehouse/dfs/dfs-namenode-secondary@.service', 
            'linux/systemd/warehouse/dfs/dfs@.service', 
            'linux/systemd/warehouse/warehouse-history-tracker@.service', 
            'linux/systemd/warehouse/warehouse-nodemanager@.service', 
            'linux/systemd/warehouse/warehouse-resource-manager@.service', 
            'linux/systemd/warehouse/warehouse@.service', 
        ] 
    ),
    ('//etc/nginx/sites-available', 
        ['linux/conf/mycorona.nginx.conf'] 
    ),
    ('/'+MYC_CONFDIR, 
        ['linux/conf/mycorona.ini'] 
    ),
    ('//usr/local/sbin', 
        [
            'linux/sbin/fetch_csse_data.sh',
            'linux/sbin/import_csse_data.sh',
            'linux/sbin/mapreduce.sh',
            'linux/sbin/export_warehouse_data.sh',
            'linux/sbin/mycorona_utils.sh',
            'linux/sbin/start_jobs.sh',
        ] 
    ),
    (HADOOP_CONF_DIR, 
        HADOOP_DIST
    ),
    
]
setup(
    name='mycorona',
    version="0.1",
    url='https://verse.1337.cx/mmmmycorona',
    author='atomask88',
    author_email='joeycw328@gmail.com',
    license='MIT',
    description='Web-Hosted live statistics for the ongoing COVID-19 outbreak',
    packages=find_packages(),
    package_data={
        "mycorona.web.res.template":["*.xml"]
    },
    install_requires=["twisted","matplotlib","adjustText","ptvsd"],
    data_files=DATA_FILES,
    entry_points=ENTRY_POINTS,
    python_requires='>=3.5',
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
    ]
)
