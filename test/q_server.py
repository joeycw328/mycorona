from multiprocessing.managers import BaseManager
from queue import Queue

import threading 


class QueueManager(BaseManager): pass


class Empty(object):
    pass

if __name__=="__main__":
    queue = Queue()
    QueueManager.register('get_queue', callable=lambda:queue)

    m = QueueManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    s = m.get_server()
    print("Starting Server Queue")
    s.serve_forever()
    # server=threading.Thread(target=s.serve_forever)
    # server.start()