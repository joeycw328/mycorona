from multiprocessing.managers import BaseManager

class QueueManager(BaseManager): pass

class Empty(object):
    pass 


if __name__=="__main__":
    QueueManager.register('get_queue')

    m = QueueManager(address=('127.0.0.1', 50000), authkey=b'abracadabra')
    m.connect()
    queue = m.get_queue()

    print("Starting Printer.")
    msg=''
    while msg != "quit":
        msg=queue.get()
        print("Printer got message: %s"%str(msg))